let mapleader = ','

"Activar NERDTree
map <C-q> :NERDTreeFocus<CR>
nnoremap dn :NERDTreeClose <CR>

" Activar AutoFormat
nnoremap ff :Neoformat <CR>

"Resetear cambios en vimrc
nnoremap <Tab>r :source $MYVIMRC <CR>

"Estados del archivo
nnoremap <Bar>w :w <CR>
nnoremap <Bar>q :q <CR>
nnoremap <Bar>x :wq <CR>

"Manejo de las tabs
nnoremap <S-Tab> <<
inoremap <S-Tab> <C-d>

"Cerrado automatico de etiquetas
"inoremap " ""<left>
"inoremap ' ''<left>
inoremap ( ()<left>
inoremap [ []<left>
inoremap { {}<left>
inoremap {<CR> {<CR>}<ESC>O
inoremap {;<CR> {<CR>};<ESC>O

"Tabs de archivos
nnoremap ta :tab ball <CR>
nnoremap tl :tabNext <CR>
nnoremap th :tabprevious <CR>
nnoremap tml :tabmove +1 <CR>
nnoremap tmh :tabmove -1 <CR>

"Buffer de archivos
nnoremap <leader>bd :bd <CR>
nnoremap <C-l> :bn <CR>
nnoremap <C-h> :bp <CR>

"Ventanas split
"nnoremap ze :sp <CR>
"nnoremap zq :vsp <CR>
"nnoremap <S-J> <C-W><C-J>
"nnoremap <S-K> <C-W><C-K>
"nnoremap <S-L> <C-W><C-L>
"nnoremap <S-H> <C-W><C-H>
nnoremap <S-Left> :vertical resize -5 <CR>
nnoremap <S-Down> :resize +5 <CR>
nnoremap <S-Up> :resize -5 <CR>
nnoremap <S-Right> :vertical resize +5 <CR>
nnoremap zzz :winc = <CR>

"Atajos para Insertar
inoremap <C-l> <Right>
inoremap <C-h> <Left>
inoremap <C-j> <Down>
inoremap <C-k> <Up>
inoremap <C-x> <Del>
inoremap <C-d> <ESC>ddi

" Copiar y pegar fuera de buffers vim
vnoremap <C-y> "+y
map <C-p> "+p

" Compilar documentos
map <leader>doc :AsyncRun . /home/hakyn/codigo/scripts/bash/doc-compile/main.sh %:p <CR>
map <leader>pum :AsyncRun . /home/hakyn/codigo/scripts/bash/doc-compile/main.sh %:p <CR>

"nnoremap <M-C-Down> :echo "HAKYN SEYER" <CR>añ
"noremap <M-Up> :echo "HOLA MUNDO" <CR>

" Editar campos <@@@>
inoremap ¬ <Esc>/<@@@><Enter>"_c5l

" Desactivar highlight busquedas
nnoremap mm :noh <CR>
