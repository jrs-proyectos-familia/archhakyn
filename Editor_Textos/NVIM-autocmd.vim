"************************************************
" File: Autocmd.vim
" Description: Comentarios en cada linea autocmd para saber que es lo que hace cada acción definida
" Keys: AltGr + ~dd
"************************************************
function CommentAutocmd()
    let c_line=line(".")
    call setline(c_line, "\"************************************************")
    call append(c_line, "\" File: ")
    call append(c_line + 1, "\" Description: <@@@>")
    call append(c_line + 2, "\" Keys: <@@@>")
    call append(c_line + 3, "\"************************************************")
    call cursor(c_line + 1, 9)
endfunction
autocmd BufEnter autocmd.vim inoremap ~ðð <ESC>:call CommentAutocmd()<ENTER>A


"************************************************
" File: Archivos Shell
" Description: Comentarios para los scripts shell
" Keys: AltGr + ~dd
"************************************************
function CommentsShell()
    let type_comment=trim(system("echo \"Cabecera\nFunciones\" | dmenu -i -p \"Tipo de Comentario\""))
    
    let c_line=line(".")
  
    " CABECERA
    if type_comment == "Cabecera"
        let type_header=trim(system("echo \"Header\nGlobal\nUsage\" | dmenu -i -p \"Tipo de Cabecera\""))

        " CABECERA -> HEADER
        if type_header == "Header"
            let text_figlet=trim(system("echo \"\" | dmenu -p \"Título del Script\""))

            if text_figlet != ""
                " Conseguimos el header de figlet
                exe system("echo \"". text_figlet ."\" | awk '{print toupper($0)}' | figlet | xclip -selection clipboard")
                " Pegamos lo obtenido del clipboard
                exe 'normal "+p'
                " Arreglamos el header obtenido
                exe "normal 0\<c-v>5jI# \<Esc>5jo\<Enter>"
                " Agregando otros elementos de relevancía
            
                let c_line=line(".")
                call setline(c_line, "# AUTHOR: Ing. Joaquín Reyes Sánchez [Hakyn Seyer]")
                call append(c_line, "#  EMAIL: joaquin.seyer21@gmail.com")
                let name_script=trim(system("basename $(pwd)"))
                call append(c_line + 1, "#   DATE: ".trim(system("date +\"%a-%d-%B-%Y\"")))
                call append(c_line + 2, "# SCRIPT: ".name_script)
                call append(c_line + 3, "#  NOTES: <@@@>")
                call append(c_line + 4, "# NOTICE: La utilización o alteración de este script queda bajo tu responsabilidad, el autor del script no se hace responsable de daños o mal uso que provoque dicho script al entorno en el que se ejecute")
            endif 

        " CABECERA -> GLOBAL
        elseif type_header == "Global"
            let counter_global=0
            
            call setline(c_line, "########## [ GLOBALES ] ##########")

            while counter_global != -1
                let name_global=trim(system("echo \"\" | dmenu -p \"Archivo Global\" | awk '{print tolower($0)}'"))
               
                if name_global == ""
                    break
                endif

                call append(c_line + counter_global, "# \"".name_global.".sh\": <@@@>")
                let counter_global += 1
                call append(c_line + counter_global, ". \"$(dirname $0)/".name_global.".sh\"")
                let counter_global += 1
            endwhile
            
            call append(c_line + counter_global, "########## [ GLOBALES ] ##########")

        " CABECERA -> USAGE
        elseif type_header == "Usage"
            let new_usage=trim(system("echo \"Nuevo\nOpciones\" | dmenu -i -p \"Crear Usage\""))
            let counter_usage=0

            if new_usage == "Nuevo"
                call setline(c_line, "########## [ USAGE ] ##########")

                " Creando el Menu
                call append(c_line + counter_usage, "USAGE=\"$(basename \"$(pwd)\") -- ".trim(system("echo \"\" | dmenu -p \"Descripción del Script\"")))
                let counter_usage += 1
                call append(c_line + counter_usage, "")
                let counter_usage += 1
                call append(c_line + counter_usage, "Uso: ")
                let counter_usage += 1
                call append(c_line + counter_usage, "\t".trim(system("basename $(pwd)")). " [Flag] [Arg]")
                let counter_usage += 1
                call append(c_line + counter_usage, "")
                let counter_usage += 1
                call append(c_line + counter_usage, "Default: ")
                let counter_usage += 1
                call append(c_line + counter_usage, "\t".trim(system("basename $(pwd)")). " <@@@>")
                let counter_usage += 1
                call append(c_line + counter_usage, "")
                let counter_usage += 1
                call append(c_line + counter_usage, "Opciones: ")
                let counter_usage += 1
                call append(c_line + counter_usage, "  |Flag|  |Arg|   |Name|         |Description| ")
                let counter_usage += 1

                " Creando la Ayuda del script
                call append(c_line + counter_usage, "\t-h             help           Muestra las especificaciones y modo de uso del script")
                let counter_usage += 1
            endif

            while 0 != 1
                let flag_letter=trim(system("echo \"\" | dmenu -p \"Letra del Flag\"")) 
                
                if flag_letter == ""
                    break
                endif
                 
                let name_flag=trim(system("echo \"\" | dmenu -p \"Nombre del Flag: ".flag_letter."\" | awk '{print tolower($0)}' | cut -c -12"))
                if name_flag == ""
                    break
                endif

                let size_name = len(name_flag)
                if size_name < 12
                    let lose_chars = 12 - size_name
                    let c = 0
                    
                    while c < lose_chars
                        let name_flag .= " "
                        let c += 1
                    endwhile
                endif

                if trim(system("echo \"Si\nNo\" | dmenu  -i -p \"Argumento del Flag: ".flag_letter."\"")) == "Si"
                    let arg_frag="*"
                else
                    let arg_frag=" "
                endif

                let desc_flag=trim(system("echo \"\" | dmenu -p \"Descripción del Flag: ". flag_letter."\""))

                if desc_flag == ""
                    break
                endif

                if new_usage == "Opciones"
                    call setline(c_line, "\t-".flag_letter."       ".arg_frag."     ".name_flag."   ".desc_flag)
                    break
                else
                    call append(c_line + counter_usage, "\t-".flag_letter."       ".arg_frag."     ".name_flag."   ".desc_flag)
                endif
                    
                let counter_usage += 1
            endwhile

            if new_usage == "Nuevo"
                call append(c_line + counter_usage, "\"")
                let counter_usage += 1

                " Agregando Getopts
                call append(c_line + counter_usage, "")
                let counter_usage += 1
                call append(c_line + counter_usage, "while getopts ':h' option; do")
                let counter_usage += 1
                call append(c_line + counter_usage, "\t case \"$option\" in")
                let counter_usage += 1
                call append(c_line + counter_usage, "\th)")
                let counter_usage += 1
                call append(c_line + counter_usage, "\t echo \"$USAGE\"")
                let counter_usage += 1
                call append(c_line + counter_usage, "exit 0")
                let counter_usage += 1
                call append(c_line + counter_usage, ";;")
                let counter_usage += 1
                call append(c_line + counter_usage, "\?)")
                let counter_usage += 1
                call append(c_line + counter_usage, "\t echo -e \"Flag -$OPTARG no disponible.\nPrueba en usar: ".trim(system("basename $(pwd)"))." -h\"")
                let counter_usage += 1
                call append(c_line + counter_usage, "exit 1")
                let counter_usage += 1
                call append(c_line + counter_usage, ";;")
                let counter_usage += 1
                call append(c_line + counter_usage, ":)")
                let counter_usage += 1
                call append(c_line + counter_usage, "\t echo -e \"Falta de argumentos para el flag -$OPTARG.\nPrueba en usar: ".trim(system("basename $(pwd)"))." -h\"")
                let counter_usage += 1
                call append(c_line + counter_usage, "exit 1")
                let counter_usage += 1
                call append(c_line + counter_usage, ";;")
                let counter_usage += 1
                call append(c_line + counter_usage, "esac")
                let counter_usage += 1
                call append(c_line + counter_usage, "done")
                let counter_usage += 1
                call append(c_line + counter_usage, "shift $((OPTIND-1)) # Reiniciar valor")
                let counter_usage += 1
                call append(c_line + counter_usage, "")
                let counter_usage += 1
                call append(c_line + counter_usage, "if [ -n \"$1\" ]")
                let counter_usage += 1
                call append(c_line + counter_usage, "then")
                let counter_usage += 1
                call append(c_line + counter_usage, "notify-send -u critical -i error \"Acción no permitida... revisa la ayuda\" && echo \"$USAGE\" && exit 1")
                let counter_usage += 1
                call append(c_line + counter_usage, "else")
                let counter_usage += 1
                call append(c_line + counter_usage, "echo \"$USAGE\" && exit 0")
                let counter_usage += 1
                call append(c_line + counter_usage, "fi")
                let counter_usage += 1

                call append(c_line + counter_usage, "########## [ USAGE ] ##########")

                exe "normal ff"
            endif
        endif

    " FUNCIONES
    elseif type_comment == "Funciones"
        call setline(c_line, "####################################################")
        call append(c_line, "# STATE: ".trim(system("echo \"WORKING\nDEPRECATED\nTODO\" | dmenu -i -p \"Estado de la función\"")))
        call append(c_line + 1, "# DESCRIPTION: <@@@>")
        call append(c_line + 2, "# PARAMS: 1) <@@@> 2) <@@@> 3) <@@@>")
        call append(c_line + 3, "# RETURN: <@@@>")
        call append(c_line + 4, "####################################################")
        call cursor(c_line, 1)
    endif
endfunction
autocmd BufEnter *.sh imap ~ðð <ESC>:call CommentsShell()<ENTER>i¬


"************************************************
" File: Scrpt hs-system
" Description: Agregar el template para insertar una nueva aplicación
" Keys: AltGr + ~a
"************************************************
autocmd BufEnter /home/hakyn/codigo/scripts/sh/hs-system/apps.txt imap ~æ Name=<Enter>Exec=<@@@><Enter>Comment=<@@@><Esc>2kA


"************************************************
" File: Script git-arch-hakyn
" Description: Agregar elementos al archivo files.json
" Keys: AltGr + ~aa
"************************************************
function ContentGitArch()
    let type_content=trim(system("echo \"Element\nScript\" | dmenu -i -p \"Tipo de Contenido\""))

    let c_line=line(".")

    if type_content == "Script"
       call setline(c_line, "{") 
       call append(c_line, "\"Name\": \"<@@@>\",")
       call append(c_line + 1, "\"Notes\": \"<@@@>\",")
       call append(c_line + 2, "\"Folder\": \"/home/hakyn/codigo/scripts/<@@@>/<@@@>\"")
       call append(c_line + 3, "},")
       call cursor(c_line, 1)
    elseif type_content == "Element"
        " Solo contiene el contenido de un Elemento
        call setline(c_line, "{")
        call append(c_line, "\"Name\": \"<@@@>\",")
        call append(c_line + 1, "\"Notes\": \"<@@@>\",")
        call append(c_line + 2, "\"Files\": [")
        call append(c_line + 3, "\"<@@@>\"")
        call append(c_line + 4, "]")
        call append(c_line + 5, "},")
        call cursor(c_line, 1)
    endif
endfunction
autocmd BufEnter /home/hakyn/codigo/scripts/bash/git-arch-hakyn/files.json imap ~æ <ESC>:call ContentGitArch()<ENTER>ffi¬

