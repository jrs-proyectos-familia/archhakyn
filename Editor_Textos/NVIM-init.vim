" => Configuración de las Lineas
set number
set relativenumber

" => Manejo de Tabs
set smartindent
set tabstop=3
set expandtab
set shiftwidth=4

" Atajos de Teclado
source $HOME/.config/nvim/init/maps.vim

" Instalador de plugins con VIM_PLUG
call plug#begin('$HOME/.config/nvim/plugged')

source $HOME/.config/nvim/init/plugins.vim

call plug#end()

" Manejo de temas para NVIM

set termguicolors
syntax on
colorscheme srcery
" Transparencía
hi Normal ctermbg=none

" Para poder abrir buffers nuevos en buffers modificados sin tener que guardar
set hidden

" Habilitar el Folding de los archivos
"autocmd BufWinLeave *.* mkview
autocmd BufWinEnter *.* silent! loadview

" Autocmd Files
source $HOME/.config/nvim/init/autocmd.vim
