Grupo: Editor_Textos
Elemento: nvim
Cambio:15-mayo-2020

Archivos Tratados: [
  "/home/hakyn/.config/nvim/init.vim",
  "/home/hakyn/.config/nvim/init/maps.vim",
  "/home/hakyn/.config/nvim/init/plugins.vim",
  "/home/hakyn/.config/nvim/init/autocmd.vim"
]

Archivos Nuevos:


Archivos Actualizados:
/home/hakyn/.config/nvim/init/plugins.vim
/home/hakyn/.config/nvim/init/autocmd.vim


Archivos Removidos:


Notas:
 Editor de textos para la terminal, es un fork de vim
