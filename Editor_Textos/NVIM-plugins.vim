" Syntastic | Para adquir la sintaxis dependiendo     de cada lenguaje
Plug 'scrooloose/syntastic'
 
" CTRLP | Menu de busqueda de archivos
Plug 'kien/ctrlp.vim'
let g:ctrlp_map = '<c-a>'

" Airline | Mejora la apariencia de la barra de esta debajo de vim
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = ''
let g:airline#extensions#tabline#formatter = 'default'

" Tema
Plug 'srcery-colors/srcery-vim'

" A.L.E
Plug 'dense-analysis/ale'
let b:ale_fixers = ['eslint']

" Para dar formato al multiples archivos de distintos tipos
Plug 'sbdchd/neoformat'

" Para el manejo de tablas en Vim
Plug 'dhruvasagar/vim-table-mode'
let g:table_mode_corner_corner='+'
let g:table_mode_header_fillchar='='

" Correr scripts en modo asincrono
Plug 'skywind3000/asyncrun.vim'

" Plugin que añade algunas funcionalidades para el tratamiento de textos
Plug 'tpope/vim-surround'

" Plugin que monitoriza los cambios entre versiones GIT
Plug 'airblade/vim-gitgutter'

" Plugin que añade un árbol de archivos
Plug 'preservim/nerdtree'

" Color sintax para archivos plantuml
Plug 'aklt/plantuml-syntax'
