#         _          _    _ _           
# _______| |__      / \  | (_) __ _ ___ 
#|_  / __| '_ \    / _ \ | | |/ _' / __|
# / /\__ \ | | |  / ___ \| | | (_| \__ \
#/___|___/_| |_| /_/   \_\_|_|\__|_|___/
#

# zsh
alias zsh-conf="$EDITOR ~/.zshrc"
alias zsh-alias="$EDITOR ~/.config/zsh/alias.zsh"
alias zsh-env="$EDITOR ~/.config/zsh/env.zsh"
alias zsh-shortcuts="$EDITOR ~/.config/zsh/shortcuts.zsh"

# i3
alias i3-conf=$EDITOR" ~/.config/i3/config"

# bspwm
alias bsp-conf="$EDITOR ~/.config/bspwm/bspwmrc"
alias bsp-shortcuts="$EDITOR ~/.config/sxhkd/sxhkdrc"
alias bsp-polybar="$EDITOR ~/.config/polybar/config"

# mariaDB
alias maria-start="sudo systemctl start mariadb.service"
alias maria-stop="sudo systemctl stop mariadb.service"
alias maria-restart="sudo systemctl restart mariadb.service"
alias maria-status="sudo systemctl status mariadb.service"

# vim
alias v="$EDITOR"
alias vim-conf="$EDITOR ~/.config/nvim/init.vim"
alias vim-maps="$EDITOR ~/.config/nvim/init/maps.vim"
alias vim-plugins="$EDITOR ~/.config/nvim/init/plugins.vim"
alias vim-autocmd="$EDITOR ~/.config/nvim/init/autocmd.vim"

# libnda
alias libnda="libnda-launch i3-command-floating"

# apache
alias apache-conf="sudo "$EDITOR" /etc/httpd/conf/httpd.conf"
alias apache-vhosts="sudo "$EDITOR" /etc/httpd/conf/extra/vhosts/hsVHosts.conf"
alias apache-domains="sudo "$EDITOR" /etc/hosts"
alias apache-status="sudo systemctl status httpd"
alias apache-stop="sudo systemctl stop httpd"
alias apache-start="sudo systemctl start httpd"
alias apache-restart="sudo systemctl restart httpd"
alias apache-proy="cd /srv/http"

# php
alias php-conf="sudo "$EDITOR" /etc/php/php.ini"

# NCMPCPP
alias mus="ncmpcpp"
alias mus-bindings="$EDITOR ~/.ncmpcpp/bindings"
alias mus-conf="$EDITOR ~/.ncmpcpp/config"

# rkhunter
alias rk-check="sudo rkhunter --check --rwo"

# ufw
alias ufw-status="sudo ufw status"
alias ufw-enable="sudo ufw enable"
alias ufw-disable="sudo ufw disable"

# ffmpeg
alias ff-dirImg="cd ~/imagenes/capturas"
alias ff-dirVid="cd ~/videos/capturas"
alias ff-dirYT="cd ~/videos/youtube"
alias ff-dirAud="cd ~/audios/capturas"

# rmtrash
alias del='trash-put'

# Dunst
alias dunst-conf="$EDITOR ~/.config/dunst/dunstrc"

# NeoMutt
alias mutt="neomutt"

# NVM
alias nvm-exec=". .nvm/nvm.sh"

# Codigo
alias sc-dirBash="cd $HOME/codigo/scripts/bash"
alias sc-dirSh="cd $HOME/codigo/scripts/sh"
alias jv-dirProy="cd $HOME/codigo/java/proyectos"
alias ru-dirProy="cd $HOME/codigo/rust/proyectos"
alias ru-dirApr="cd $HOME/codigo/rust/aprendiendo"

SCRIPTS_DIR="$HOME/codigo/scripts"
# Bash-Scripts
alias bsp-external-rules="$SCRIPTS_DIR/bash/bsp-external-rules/main.sh"
alias doc-compile="$SCRIPTS_DIR/bash/doc-compile/main.sh"
alias ff-record="$SCRIPTS_DIR/bash/ff-record/main.sh"
alias i3-open-apps="$SCRIPTS_DIR/bash/i3-open-apps/main.sh"
alias poly-modules="$SCRIPTS_DIR/bash/poly-modules/main.sh"
alias cut-media="$SCRIPTS_DIR/bash/cut-media/main.sh"
alias feh-action="$SCRIPTS_DIR/bash/feh-action/main.sh"
alias git-arch-hakyn="$SCRIPTS_DIR/bash/git-arch-hakyn/main.sh"
alias libnda-launch="$SCRIPTS_DIR/bash/libnda-launch/main.sh"
alias wac-tablet="$SCRIPTS_DIR/bash/wac-tablet/main.sh"
#Sh-Scripts
alias hs-system="$SCRIPTS_DIR/sh/hs-system/main.sh"
alias mu-play="$SCRIPTS_DIR/sh/mu-play/main.sh"
alias mc-hakynland="$SCRIPTS_DIR/sh/mc-hakynland/main.sh"
