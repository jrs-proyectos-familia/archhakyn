# Path to your oh-my-bash installation.
export OSH=/home/hakyn/.oh-my-bash

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-bash is loaded.
OSH_THEME="agnoster"
#OSH_THEME="steeef"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_OSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $OSH/custom?
# OSH_CUSTOM=/path/to/new-custom-folder

# Which completions would you like to load? (completions can be found in ~/.oh-my-bash/completions/*)
# Custom completions may be added to ~/.oh-my-bash/custom/completions/
# Example format: completions=(ssh git bundler gem pip pip3)
# Add wisely, as too many completions slow down shell startup.
completions=(
  git
  composer
  ssh
)

# Which aliases would you like to load? (aliases can be found in ~/.oh-my-bash/aliases/*)
# Custom aliases may be added to ~/.oh-my-bash/custom/aliases/
# Example format: aliases=(vagrant composer git-avh)
# Add wisely, as too many aliases slow down shell startup.
aliases=(
  general
)

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-bash/plugins/*)
# Custom plugins may be added to ~/.oh-my-bash/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git
  bashmarks
)

source $OSH/oh-my-bash.sh

# User configuration
# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-bash libs,
# plugins, and themes. Aliases can be placed here, though oh-my-bash
# users are encouraged to define aliases within the OSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias bashconfig="mate ~/.bashrc"
# alias ohmybash="mate ~/.oh-my-bash"

#=========================================================
#            CONFIGURACION HAKYN SEYER
#=========================================================

# Alias del sistema
alias bashConf="vim ~/.bashrc"

alias i3Conf="vim ~/.config/i3/config"

alias mariaStart="sudo systemctl start mariadb.service"
alias mariaStop="sudo systemctl stop mariadb.service"
alias mariaRestart="sudo systemctl restart mariadb.service"
alias mariaStatus="sudo systemctl status mariadb.service"

alias vimConf="vim ~/.vimrc"

alias libnda="java -jar ~/Aplicaciones/Libnda/libnda-B1.0.1.jar"

alias apacheConf="sudo vim /etc/httpd/conf/httpd.conf"
alias apacheVHosts="cd /etc/httpd/conf/extra/vhosts/"
alias apacheHosts="sudo vim /etc/hosts"
alias apacheStatus="sudo systemctl status httpd"
alias apacheStop="sudo systemctl stop httpd"
alias apacheStart="sudo systemctl start httpd"
alias apacheRestart="sudo systemctl restart httpd"
alias apacheProy="cd /srv/http"

alias phpConf="sudo vim /etc/php/php.ini"
alias phpProy="cd $HOME/Codigo/Php/Proyectos"

alias hsMus="ncmpcpp"

alias kittyConf="vim ~/.config/kitty/kitty.conf"

alias rkCheck='sudo rkhunter --check --rwo'

alias holas="echo JOAQUIN"

# Variables de entorno
export JAVA_HOME="/usr/lib/jvm/java-8-openjdk"

export PATH=$JAVA_HOME/bin:/opt/gradle/gradle-4.10.2/bin:/home/hakyn/Codigo/Scripts/Bin:$PATH

export EDITOR="vim"

# NVM manejador de versiones para node
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
