#         _       _   _ ____  
# _______| |__   | | | / ___| 
#|_  / __| '_ \  | |_| \___ \ 
# / /\__ \ | | | |  _  |___) |
#/___|___/_| |_| |_| |_|____/
#

# Información git
autoload -Uz vcs_info
precmd() { vcs_info }
zstyle ':vcs_info:git*' formats ' %b'
setopt prompt_subst


# Personalizacion Promt
export PS1='%{%F{magenta}%}%B%n %{%F{yellow}%}● %{%F{cyan}%}%2~ %{%F{green}%}${vcs_info_msg_0_} %{%F{yellow}%}$ %{%F{white}%}%b'

# ShortCuts
source ~/.config/zsh/shortcuts.zsh

# Variables de Entorno
source ~/.config/zsh/env.zsh

# Alias
source ~/.config/zsh/alias.zsh

# LS_COLORS
LS_COLORS=$LS_COLORS:'di=1;32:fi=0;37:ex=1;36:*.zip=0;33:*.rar=0;33:*.tar=0;33:*.png=0;34:*.jpg=0;34:*.mp4=1;34' ; export LS_COLORS
alias ls='ls --color'
