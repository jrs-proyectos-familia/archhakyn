#         _       ____  _   _  ___  ____ _____ ____ _   _ _____ ____  
# _______| |__   / ___|| | | |/ _ \|  _ \_   _/ ___| | | |_   _/ ___| 
#|_  / __| '_ \  \___ \| |_| | | | | |_) || || |   | | | | | | \___ \ 
# / /\__ \ | | |  ___) |  _  | |_| |  _ < | || |___| |_| | | |  ___) |
#/___|___/_| |_| |____/|_| |_|\___/|_| \_\|_| \____|\___/  |_| |____/ 
#

# Inicio de la linea
bindkey "^[0" vi-beginning-of-line
# Fin de la linea
bindkey "^[$" vi-end-of-line
# Saltar hacia delante de palabra en palabra
bindkey "^[w" vi-forward-word
# Saltar hacia atras de palabra en palabra
bindkey "^[b" vi-backward-word
# Mover hacia delante letra por letra
bindkey "^[l" vi-forward-char
# Mover hacia atras letra por letra
bindkey "^[h" vi-backward-char
# Remover letras delante
bindkey "^[x" vi-delete-char


