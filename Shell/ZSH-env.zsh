# _____ _   ___     __ __     ___    ____  ___    _    ____  _     _____ ____  
#| ____| \ | \ \   / / \ \   / / \  |  _ \|_ _|  / \  | __ )| |   | ____/ ___| 
#|  _| |  \| |\ \ / /   \ \ / / _ \ | |_) || |  / _ \ |  _ \| |   |  _| \___ \ 
#| |___| |\  | \ V /     \ V / ___ \|  _ < | | / ___ \| |_) | |___| |___ ___) |
#|_____|_| \_|  \_/       \_/_/   \_\_| \_\___/_/   \_\____/|_____|_____|____/ 
#

# Java
JAVA_USE=8
JAVA_ROUTE="/usr/lib/jvm"
#JAVAFX_ROUTE="/opt/javaFX"
if [ "$JAVA_USE" = 13 ]
then
    export JAVA_HOME="$JAVA_ROUTE/java-13-openjdk"
elif [ "$JAVA_USE" = 11 ]
then
    export JAVA_HOME="$JAVA_ROUTE/java-11-openjdk"
else
    export JAVA_HOME="$JAVA_ROUTE/java-8-openjdk"
fi

# Gradle
GRADLE_USE=6
if [ "$GRADLE_USE" = 6 ]
then
    GRADLE="/opt/gradle/gradle-6.1/bin"
else
    GRADLE="/opt/gradle/gradle-4.10.2/bin"
fi

# Editor
export EDITOR="nvim"

# Terminal
export TERMINAL="alacritty --working-directory"

# Musica Sistema
#export HS_MUSICA="/home/hakyn/musica"

# Rust (Lenguaje de programación)
RUST="$HOME/.cargo/bin"

# PATH
export PATH=$JAVA_HOME/bin:$GRADLE:$SCRIPTS_BASH:$RUST:$PATH

