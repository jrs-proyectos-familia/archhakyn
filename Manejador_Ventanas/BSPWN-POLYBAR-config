;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/polybar/polybar
;
;   The README contains a lot of information
;
;==========================================================

[colors]
    ; Color de Fondo "Escritorios"
    background-light = #4a4a40

    ; Color de Fondo de la barra
    background = #1C1B19

    ; Color del texto (Color suave)
    foreground-light = #f5f7d4

    ; Color del texto (Color neutral)
    foreground = #f0f2bc

    ; Color del texto (Color fuerte)
    foreground-hard = #ebf300

[bar/example]
    ; Idioma
    locale = es_MX.UTF-8
        
    override-redirect = true
    
    ; Monitores
    ;monitor = ${env:MONITOR:HDMI-1}
        
    ; Dimensiones de la barra
    width = 100%
    height = 21
        
    ; Offset de la barra
    ;offset-x = 1%
    ;offset-y = 1%
        
    ; Border redondeado de la barra
    radius = 0
    
    fixed-center = false
        
    ; Background y Foreground de la barra
    background = ${colors.background}
    foreground = ${colors.foreground}
        
    ; Tamaño y color de las lineas
    line-size = 3
    line-color = #f00
        
    ; Borde y color del borde de la barra
    border-size = 0
    ;border-color = #00000000
        
    ; Padding de la barra
    padding-left = 2
    padding-right = 2
        
    ; Margen entre módulos
    module-margin = 0

    ; Separador de módulos
    separator = "%{F#686a15} | %{F-}"
    
    ; Fuentes usadas en la barra
    font-0 = fixed:pixelsize=8.5;1
    font-1 = unifont:fontformat=truetype:size=8:antialias=false;0
    font-2 = siji:pixelsize=10;1
    font-3 = Hack Nerd Font Mono:pixelsize=13;2
        
    ; Módulos del sistema
    modules-left = bspwm
    modules-center = ncmpcpp
    modules-right = caps nums memory xbacklight pulseaudio date 
        
    ; Position del tray "Demonios bateria wifi"
    tray-position = right
    tray-padding = 2
    tray-background = #f9346c
    
    ;wm-restack = bspwm
    
    ;override-redirect = true
    
    ;scroll-up = bspwm-desknext
    ;scroll-down = bspwm-deskprev
    
    cursor-click = pointer
    cursor-scroll = pointer

; __  __  ___  ___  _   _ _    ___ ___
;|  \/  |/ _ \|   \| | | | |  | __/ __|
;| |\/| | (_) | |) | |_| | |__| _|\__ \
;|_|  |_|\___/|___/ \___/|____|___|___/

[module/bspwm]
    type = internal/bspwm
    
    label-focused = 
    label-focused-background = ${colors.background-light}
    label-focused-foreground = ${colors.foreground}
    label-focused-padding = 2
    
    label-occupied = 
    label-occupied-padding = 2
    
    label-urgent = 
    label-urgent-background = ${colors.foreground}
    label-urgent-padding = 2
    
    label-empty = 
    label-empty-foreground = ${colors.foreground}
    label-empty-padding = 2

[module/xbacklight]
    type = internal/xbacklight
    
    enable-scroll = false
    
    format = <ramp> <label>
    label = %percentage%%
    
    ramp-4 = 
    ramp-3 = 
    ramp-2 = 
    ramp-1 = 
    ramp-0 = 

[module/date]
    type = internal/date
    interval = 5
    
    date = " %d-%m-%Y"
    date-alt = %A %d-%B-%Y
    
    time = %I:%M:%S %p
    time-alt = %I:%M %p
    
    format-prefix = " " 
    
    label = %date% %time%

[module/pulseaudio]
    type = internal/pulseaudio
    
    format-volume = <ramp-volume> <label-volume>
    label-volume =  %percentage%%
    
    label-muted = 婢 0%
    label-muted-foreground = ${colors.foreground-hard}
    
    ramp-volume-2 =   
    ramp-volume-1 = 墳 
    ramp-volume-0 = 奔 

; ___  ___ ___ ___ ___ _____ ___
;/ __|/ __| _ \_ _| _ \_   _/ __|
;\__ \ (__|   /| ||  _/ | | \__ \
;|___/\___|_|_\___|_|   |_| |___/

[module/ncmpcpp]
    type = custom/script
    exec = /home/hakyn/codigo/scripts/bash/poly-modules/main.sh -n
    interval = 25

[module/memory]
    type = custom/script
    exec = /home/hakyn/codigo/scripts/bash/poly-modules/main.sh -m

    tail = true
    click-left = kill -USR1 %pid%

    format-margin = 1

[module/caps]
    type = custom/script
    exec = echo "%{F#686a15}%{B#f5f7d4}  "
    exec-if = "[ "$(xset -q | grep Caps | awk '{print $4}')" = "on" ]"
    interval = 1

[module/nums]
    type = custom/script
    exec = echo "%{F#686a15}%{B#f5f7d4}  "
    exec-if = "[ "$(xset -q | grep Caps | awk '{print $8}')" = "on" ]"
    interval = 1
