Grupo: Manejador_Ventanas
Elemento: bspwn
Cambio:03-marzo-2020

Archivos Tratados: [
  "/home/hakyn/.config/bspwm/bspwmrc",
  "/home/hakyn/.config/sxhkd/sxhkdrc",
  "/home/hakyn/.config/polybar/config polybar",
  "/home/hakyn/.config/polybar/launch.sh polybar"
]

Archivos Nuevos:


Archivos Actualizados:
/home/hakyn/.config/bspwm/bspwmrc
/home/hakyn/.config/sxhkd/sxhkdrc
/home/hakyn/.config/polybar/config polybar


Archivos Removidos:


Notas:
 Manejador de ventanas tipo tiling. Actualmente lo uso. Requiere mayor tiempo para configurar que i3 a mi punto de vista, pero el modo de manejar las ventanas es realmente muy bueno a comparación de i3 (en mi opinion). Hay poca documentación con respecto a i3
