#============================================================
#              CONFIGURACIONES BASICAS
#============================================================

 # Tecla "MOD"
 set $mod Mod4

 # Fuente para el escritorio
 font pango:firacode 10

 # Habilitar el movimiento de ventanas flotantes usando el ratón
 floating_modifier $mod

 # Recargar la configuración i3
 bindsym $mod+Shift+c reload
 
 #Reiniciar i3
 bindsym $mod+Shift+r restart

 # Cerrar sesión i3
 bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'Salir del sistema i3. Confirma para salir' -B 'Si, exit i3' 'i3-msg exit'"

 # Teclas de sonido
 bindsym XF86AudioRaiseVolume exec amixer -q -D pulse sset Master 5%+; exec pkill -RTMIN+10 i3blocks 
 bindsym XF86AudioLowerVolume exec amixer -q -D pulse sset Master 5%-; exec pkill -RTMIN+10 i3blocks
 bindsym XF86AudioMute exec amixer -q -D pulse sset Master toggle; exec pkill -RTMIN+10 

#============================================================
#              CONFIGURACIONES PROGRAMAS
#============================================================
 
 # Terminal
 #bindsym $mod+Return exec kitty
 bindsym $mod+Return exec urxvt
 
 # DMenu
 bindsym $mod+Shift+d exec dmenu_run

 # Rofi
 #bindsym $mod+d exec rofi -show run -theme purple -opacity "80" -columns 2 -font "Open Sans light 10" -show-icons
 bindsym $mod+d exec rofi -modi run -show drun -columns 2 -theme purple -opacity "90" -font "Open Sans light 11" -show-icons

 # Firefox 
 bindsym $mod+Control+q exec firefox
 # Pale Moon
 bindsym $mod+Control+w exec palemoon
 
 # Arandr
 bindsym $mod+Control+0 exec arandr

 # Pavucontrol
 bindsym $mod+Control+9 exec pavucontrol

 # Pcmanfm
 bindsym $mod+Control+m exec pcmanfm

 # Ncmpcpp
 # bindsym $mod+Control+m exec ncmpcpp
 
 # i3Lock
 bindsym $mod+x exec i3lock -i  $HOME/imagenes/wallpapers/universo/universo_purpura.png

 # Captura de pantalla
 bindsym $mod+F1 exec --no-startup-id $HOME/codigo/scripts/bash/ff-record/main.sh desktop
 
 # Realizar grabación
 bindsym $mod+F2 exec --no-startup-id $HOME/codigo/scripts/bash/ff-record/main.sh memory
 # Detener última grabación
 bindsym $mod+Shift+F2 exec --no-startup-id $HOME/codigo/scripts/bash/ff-record/ff-kill.sh last

 #==== PROGRAMAS EJECUTADOS AL INICIAR i3

 # TouchPad
 exec --no-startup-id synclient TapButton1=1 TapButton2=3 TapButton3=2

 # Bateria
 exec --no-startup-id mate-power-manager
 
 # Wifi
 exec --no-startup-id nm-applet

 # MPD
 exec --no-startup-id mpd

 # Transparencia Compton
 exec --no-startup-id compton

 # Fondo de pantalla
 #exec --no-startup-id nitrogen --restore

 # Fondo de pantalla (COLOR)
 exec --no-startup-id xsetroot -solid "#2f1e2e"

#============================================================
#              CONFIGURACIONES VENTANAS
#============================================================

 # Cerrar ventanas
 bindsym $mod+Shift+q kill 

 # Foco entre ventanas
 bindsym $mod+h focus left
 bindsym $mod+j focus down
 bindsym $mod+k focus up
 bindsym $mod+l focus right

 # Mover entre ventanas
 bindsym $mod+Shift+h move left
 bindsym $mod+Shift+j move down
 bindsym $mod+Shift+k move up
 bindsym $mod+Shift+l move right

 # Orientación de las ventanas nuevas
 bindsym $mod+q split h
 bindsym $mod+e split v

 # Fullscreen
 bindsym $mod+f fullscreen toggle

 # Cambiar el contenedor del layout
 bindsym $mod+m layout stacking
 bindsym $mod+b layout tabbed
 bindsym $mod+Shift+n layout toggle split
 
 # Flotante en ventanas
 bindsym $mod+space floating toggle
 bindsym $mod+shift+space focus mode_toggle
 
 # Focus a todo el contenido del escritorio actual
 bindsym $mod+a focus parent

 # Redimencionar las ventanas
 mode "resize" {
  bindsym l resize shrink width 10 px or 10 ppt
  bindsym j resize grow height 10 px or 10 ppt
  bindsym k resize shrink height 10 px or 10 ppt
  bindsym h resize grow width 10 px or 10 ppt

  bindsym Return mode "default"
  bindsym Escape mode "default"
  bindsym $mod+r mode "default"
 }
 bindsym $mod+r mode "resize"
 
#============================================================
#              CONFIGURACIONES ESCRITORIOS
#============================================================

 # Nombre a los escritorios
 set $ws1 ""
 set $ws2 ""
 set $ws3 ""
 set $ws4 ""
 set $ws5 ""
 set $ws6 ""
 set $ws7 ""
 set $ws8 ""
 set $ws9 ""
 set $ws10 ""

 # Cambiar entre escritorios
 bindsym $mod+1 workspace $ws1
 bindsym $mod+2 workspace $ws2
 bindsym $mod+3 workspace $ws3
 bindsym $mod+4 workspace $ws4
 bindsym $mod+5 workspace $ws5
 bindsym $mod+6 workspace $ws6
 bindsym $mod+7 workspace $ws7
 bindsym $mod+8 workspace $ws8
 bindsym $mod+9 workspace $ws9
 bindsym $mod+0 workspace $ws10

 # Mover ventanas a escritorios
 bindsym $mod+Shift+1 move container to workspace $ws1
 bindsym $mod+Shift+2 move container to workspace $ws2
 bindsym $mod+Shift+3 move container to workspace $ws3
 bindsym $mod+Shift+4 move container to workspace $ws4
 bindsym $mod+Shift+5 move container to workspace $ws5
 bindsym $mod+Shift+6 move container to workspace $ws6
 bindsym $mod+Shift+7 move container to workspace $ws7
 bindsym $mod+Shift+8 move container to workspace $ws8
 bindsym $mod+Shift+9 move container to workspace $ws9
 bindsym $mod+Shift+0 move container to workspace $ws10

#============================================================
#              CONFIGURACIONES i3Bar
#============================================================

 bar {
  status_command i3blocks -c ~/.config/i3/i3blocks.conf
  font pango: Open Sans Light 9
  position top
  separator_symbol " "
   
  colors {
   background #2f1e2e
   separator #ffffff
   statusline #ffffff

   # focused_workspace #c6094f #c6094f #ffffff
   # active_workspace #c6094f #c6094f #ffffff
   # inactive_workspace #ffffff #ffffff #c6094f

   focused_workspace #f9346c #f9346c #ffffff
   active_workspace #f9346c #f9346c #ffffff
   inactive_workspace #2f1e2e #2f1e2e #ffffff
  }
 }

#============================================================
#              PERSONALIZACION i3
#============================================================

 # Padding entre ventanas
 for_window[class="^.*"] border pixel 4
 gaps inner 5
 gaps outer 0

 # Remover la barra de titulo de cada ventana
 for_window[class="^.*"] border pixel 1

 # Color de ventana al tener el foco
 #ROSA-OLD client.focused #c6094f #c6094f #ffffff #c6094f
 client.focused #f9346c #f9346c #ffffff #f9346c
 #client.focused #2f1e2e #2f1e2e #ffffff #2f1e2e
 client.unfocused #2f1e2e #2f1e2e #ffffff #2f1e2e #2f1e2e
 client.focused_inactive #2f1e2e #2f1e2e #ffffff #2f1e2e #2f1e2e

for_window [class=".*"] move position center
