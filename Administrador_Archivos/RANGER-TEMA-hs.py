from ranger.gui.colorscheme import ColorScheme
from ranger.gui.color import *

class hs(ColorScheme):
    progress_bar_color = green

    def use(self, context):
        fg, bg, attr = default_colors

        # attr = bold | underline
        # attr |= reverse
        # attr ^= reverse

        if context.reset:
            return default_colors

        elif context.in_browser:
            # Seleccion del elemento
            if context.selected:
                attr = reverse
            else:
                attr |= normal
                #fg = yellow

            if context.empty or context.error:
                # Errores
                bg = 9
                attr |= bold
            
            if context.border:
                fg = default
            
            if context.file:
                # Todos los archivos
                fg = white
                fg += BRIGHT

            if context.media:
                if context.image:
                    fg = blue
                else:
                    fg = blue
                    attr |= bold

            if context.document:
                fg = white
                fg += BRIGHT


            if context.container:
                # .zip .tar
                fg = yellow

            if context.directory:
                # Directorios
                attr |= bold
                fg = green
            elif context.executable and not \
                    any((context.media, context.container,
                        context.fifo, context.socket)):
                # Ejecutables
                fg = cyan
                attr |= bold
            
            if context.socket:
                fg = magenta
                attr |= bold
            if context.fifo or context.device:
                fg = yellow
                if context.device:
                    attr |= bold
            if context.link:
                fg = context.good and cyan or magenta
            
            if context.main_column:
                if context.selected:
                    attr |= bold
                if context.marked:
                    attr |= bold
                    fg = green

        elif context.in_titlebar:
            attr |= bold
            fg = cyan

            if context.hostname:
                fg = magenta
        
        elif context.in_statusbar:
            if context.permissions or context.mtime:
                # Información de permisos del archivo o carpeta y su información sobre la fecha
                attr |= bold
                fg = green


        return fg, bg, attr
