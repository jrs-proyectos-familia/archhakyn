#!/bin/bash

# Para cambiar la sensibilidad del lápiz basta con cambiar el 2do numero.
# Default 40
# n < 40 ) Lapiz más duro [ Apenas y se ve el trazo ]
# n > 40 ) Lapiz más suave [ Se ve mucho el trazo ]
xsetwacom set "Wacom One by Wacom S Pen stylus" PressureCurve 0 50 85 100
