#!/bin/bash

# Autor: Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
# Script: i3-open-apps Script para abrir ventanas en diversos modos en i3

# IMPORTANTE: La utilización o alteración de este script queda bajo tu responsabilidad, el autor del script no se hace responsable de daños o mal uso que provoque dicho script

# ____   ____ ____  ___ ____ _____
#/ ___| / ___|  _ \|_ _|  _ \_   _|
#\___ \| |   | |_) || || |_) || |
# ___) | |___|  _ < | ||  __/ | |
#|____/ \____|_| \_\___|_|    |_|
#

# TODO: FAVOR DE MODIFICAR ESTE SCRIPT EN UN FUTURO DEBIDO A QUE EL MANEJO DE PID_LIBNDA LO PUEDE REALIZAR EL SCRIPT libnda-launch

# Ruta directa del script para encontrar el PID Libnda
PID_LIBNDA="$HOME/codigo/scripts/bash/libnda-launch/pid-libnda.sh"

function open_window () {
  # $1 - ID de la ventana
  # $2 - Modo de abrir la ventana
  # $3 - Commando de la aplicación

  case "$2" in
    "floating")
      wmctrl -ia $1 && i3-msg floating enable move position center
      #wmctrl -ia $1 && i3-msg floating enable move position cursor
    ;;
    *)
    echo "No hay imprementación para $2... Quizás en un futuro"
    ;;
  esac
}

function get_id_window () {
  # $1 - PID del usuario
  # $2 - Modo de abrir la ventana

  PID_WINDOW=`wmctrl -lp | grep " $1 " | head -n 1 | awk '{print $3}'`
  ID_WINDOW=`wmctrl -lp | grep " $1 " | head -n 1 | awk '{print $1}'`

  if [ "$PID_WINDOW" = "$1" ]
  then
    open_window $ID_WINDOW $2
  else
    echo "La PID no pasó la validación de las ventanas"
  fi
}

sleep 2 # Delay para que de tiempo de cargar la ventana y así poder conseguir el ID con wmctrl

if [ "$1" != "" ]
then
  case $1 in
    ''|*[!0-9]*)
        # PID="$($HOME/codigo/scripts/bash/$1)" OBSOLETO
        PID="$(source $PID_LIBNDA)"
        get_id_window $PID $2
      ;;
    *) get_id_window $1 $2 ;;
  esac
else
  echo ERROR: No existe un PID al cual rastrear
fi
