#!/bin/bash

# Autor: Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
# Script: cut-media Script para generar cortes de archivos multimedia

# IMPORTANTE: La utilización o alteración de este script queda bajo tu responsabilidad, el autor del script no se hace responsable de daños o mal uso que provoque dicho script

# ____   ____ ____  ___ ____ _____
#/ ___| / ___|  _ \|_ _|  _ \_   _|
#\___ \| |   | |_) || || |_) || |
# ___) | |___|  _ < | ||  __/ | |
#|____/ \____|_| \_\___|_|    |_|
#

# Modifica la ruta para que apunte al archivo "var.sh"
source $HOME/codigo/scripts/bash/cut-media/var.sh

function main () {
    local CURRENT_PATH=$(pwd)
    local FILE_TYPE="$(echo "$FILE" | awk -F'.' '{print $NF}')"
    
    ffmpeg -i "$FILE" -ss "$START" -to "$END" -async 1 "$CURRENT_PATH/$TITLE.$FILE_TYPE"
}

# Validación y menu de opciones de entrada
USAGE="$(basename "$0") [-h] -- Script para realizar simples cortes de archivos multimedia.

Opciones:
    -h    [help]              Muestra las opciones a usar en el script
    -f *  [file]              Archivo a cortar (No sufre daños el archivo)
    -s *  [start]             Tiempo inicial donde partirá el corte
    -e *  [end]               Tiempo final donde parará el corte
    -t *  [title]             Titulo que llevará el corte
"

while getopts ':hf:s:e:t:' option; do
    case "$option" in
        h)
            echo "$USAGE"
            exit 0
            ;;
        f)
            FILE="$OPTARG"
            ;;
        s)
            START="$OPTARG"
            ;;
        e)
            END="$OPTARG"
            ;;
        t)
            TITLE="$OPTARG"
            ;;
        \?)
            echo -e "Flag -$OPTARG no disponible.\nPrueba en usar: cut-media -h"
            exit 1
            ;;
        :)
            echo -e "Falta de argumentos para el flag -$OPTARG.\nPrueba en usar: cut-media -h"
            exit 1
            ;;
    esac
done
shift $((OPTIND-1)) # Reiniciamos el valor del OPTIND

if [ -z "$FILE" ] || [ -z "$START" ] || [ -z "$END" ] || [ -z "$TITLE" ]
then
   notify-send -u critical -i error "Acción no permitida... revisa la ayuda" && echo "$USAGE" && exit 1
else
   main
fi

