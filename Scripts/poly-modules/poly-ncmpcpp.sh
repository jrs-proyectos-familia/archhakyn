#!/bin/bash

SONG="$(mpc current 2>/dev/null)"

ARTIST="$(echo "$SONG" | awk -F'-' '{print $1}')"
# Left Trim
ARTIST="${ARTIST#"${ARTIST%%[![:space:]]*}"}"
# Right Trim
ARTIST="${ARTIST%"${ARTIST##*[![:space:]]}"}"
    
TITLE="$(echo "$SONG" | awk -F'-' '{print $2}')"
# Left Trim
TITLE="${TITLE#"${TITLE%%[![:space:]]*}"}"
# Right Trim
TITLE="${TITLE%"${TITLE##*[![:space:]]}"}"

    
if [ ! -z "$ARTIST" ] && [ ! -z "$TITLE" ]
then
    echo "ﴁ $ARTIST   $TITLE"
else 
    echo ""
fi

