#!/bin/bash

t=0

toggle() {
    t=$(((t + 1) % 2))
}

output () {
    # $1 Nombre de la partición
    # $2 Tamaño disponible de la partición

    echo "%{F#15d8be} $1: %{F-} $2"
}


trap "toggle" USR1


while true; do
    if [ $t -eq 0 ]; then
        HOME="$( lsblk -o MOUNTPOINT,SIZE,FSUSED,FSUSE%,FSAVAIL | grep /home | awk '{print $5}')"
        output "/home" "$HOME"
    else
        MAIN="$( lsblk -o MOUNTPOINT,SIZE,FSUSED,FSUSE%,FSAVAIL | grep "^/ " | awk '{print $5}')"
        output "/" "$MAIN"
    fi
    sleep 1 &
    wait
done
