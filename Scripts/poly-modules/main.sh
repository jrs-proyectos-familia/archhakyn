#!/bin/bash

# Autor: Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
# Script: poly-modules Script que guarda diversos modulos para mostrar en la polybar de bspwm

# IMPORTANTE: La utilización o alteración de este script queda bajo tu responsabilidad, el autor del script no se hace responsable de daños o mal uso que provoque dicho script

# ____   ____ ____  ___ ____ _____
#/ ___| / ___|  _ \|_ _|  _ \_   _|
#\___ \| |   | |_) || || |_) || |
# ___) | |___|  _ < | ||  __/ | |
#|____/ \____|_| \_\___|_|    |_|
#

# Ruta principal de los modulos (MOVIDO AL ARCHIVO PRINCIPAL)
PATH_MODULES="$(echo "$0" | awk -F'/' 'NF{NF--};1' | sed -e 's/ /\//g')"

# Validación y menu de opciones de entrada
USAGE="$(basename "$0") [-h] -- Script que lanza diversos modulos para la polybar de bspwm

Modulos:
    -h   [help]       Muestra las opciones a usar en el script
    -n   [ncmpcpp]    Muestra la información de la canción en reproducción
    -m   [memoria]    Muestra la información de la memoria del sistema
"
while getopts ':hnm' option; do
    case "$option" in
        h)
            echo "$USAGE"
            exit 0
            ;;
        n)
            source "$PATH_MODULES/poly-ncmpcpp.sh"
            exit 0
            ;;
        m)
            source "$PATH_MODULES/poly-memory.sh"
            exit 0
            ;;
        \?)
            echo -e "Flag -$OPTARG no disponible.\nPrueba en usar: poly-modules -h"
            exit 1
            ;;
        :)
            echo -e "Falta de argumentos para el flag -$OPTARG.\nPrueba en usar: poly-modules -h"
            exit 1
            ;;
    esac
done

shift $((OPTIND-1)) # Reiniciamos el valor del OPTIND

if [ -n "$1" ]
then
   notify-send -u critical -i error "Acción no permitida... revisa la ayuda" && echo "$USAGE" && exit 1
else
    echo "$USAGE"
fi
