#!/bin/bash

WID=$1
CLASS=$2
INSTANCE=$3
TITLE=$(xtitle "$WID")

function webcam () {
    local SIZE="$(xdpyinfo | grep dimensions | awk '{print $2}')"
    local X=$(echo "$SIZE" | awk -F'x' '{print $1}')
    local Y=$(echo "$SIZE" | awk -F'x' '{print $2}')

    local WIDTH=265
    local HEIGHT=200

    local POSX=$((X - WIDTH))
    local POSY=$((Y - HEIGHT))
    
    echo "state=floating"
    echo "sticky=on"
    
    xdotool windowsize "$WID" $WIDTH $HEIGHT
    xdotool windowmove "$WID" $POSX $POSY
}

# Webcam
if [ "$CLASS" = "guvcview" ];then webcam
fi

