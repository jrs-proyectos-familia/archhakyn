#!/bin/bash

# Autor: Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
# Script: feh-action Script para ejecutar feh en diversas formas

# IMPORTANTE: La utilización o alteración de este script queda bajo tu responsabilidad, el autor del script no se hace responsable de daños o mal uso que provoque dicho script

# ____   ____ ____  ___ ____ _____
#/ ___| / ___|  _ \|_ _|  _ \_   _|
#\___ \| |   | |_) || || |_) || |
# ___) | |___|  _ < | ||  __/ | |
#|____/ \____|_| \_\___|_|    |_|
#

# Modifica la ruta para que apunte al archivo "var.sh"
source "$(dirname $0)/var.sh"

function main () {
    # $1 Imagen a mostrar
    local COMMAND="feh -B \"#1d1d1d\" -g "$(xdpyinfo | grep dimensions | awk '{print $2}')" "

    if [ "$RAND" = "true" ]
    then
       COMMAND+="-z " 
    fi

    if [ "$FULL" = "true" ]
    then
        COMMAND+="-F "
    fi


    if [ ! -z "$IMG" ]
    then
       COMMAND+="$IMG " 
    else
        if [ "$ALL" = "true" ]
        then
            COMMAND+="*.jpg *.png"
        elif [ ! -z "$EXT" ]
        then
            COMMAND+="$EXT "
        fi
    fi

    eval $COMMAND
}

USAGE="$(basename "$0") [-h] -- Script que ejecuta feh para distintos propositos de uso

Default:
    feh-action -h

Opciones: 
    -h    [help]         Muestra las opciones a usar en el script
    -i *  [image]        Imagen a mostrar (Solo 1 imagen)
    -a    [all images]   Tomar todas las imagenes
    -e *  [extension]    Tomar todas las images con extension
    -r    [random]       Activar modo random
    -f    [fullscreen]   Activar modo fullscreen
"
while getopts ':hi:ae:rf' option; do
    case "$option" in
        h)
            echo "$USAGE"
            exit 0
            ;;
        i)
            IMG+="$OPTARG " 
            CONTINUE="true"
            ;;
        a)
            ALL="true"
            CONTINUE="true"
            ;;
        r)
            RAND="true"
            CONTINUE="true"
            ;;
        f)
            FULL="true"
            CONTINUE="true"
            ;;
        e)
            EXT+="*.$OPTARG "
            CONTINUE="true"
            ;;
        \?)
            echo -e "Flag -$OPTARG no disponible.\nPrueba en usar: feh-action -h"
            exit 1
            ;;
        :)
            echo -e "Falta de argumentos para el flag -$OPTARG.\nPrueba en usar: feh-action -h"
            exit 1
            ;;
    esac
done
shift $((OPTIND-1)) # Reiniciamos el valor del OPTIND

if [ -n "$1" ]
then
   notify-send -u critical -i error "Acción no permitida... revisa la ayuda" && echo "$USAGE" && exit 1
else
    if [ "$CONTINUE" = "true" ]
    then
        main
    else
        echo "$USAGE"
    fi
fi

