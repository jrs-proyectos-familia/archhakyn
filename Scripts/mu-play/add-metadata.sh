#!/bin/sh

. "$(dirname $0)/get-songs.sh"

META_ARTIST=""
META_TITLE=""
META_GENDER=""
# INTENTAR AGREGAR los datos

while IFS= read -r song <&3;
do
    TMP_ARTIST=""
    TMP_TITLE=""
   
    META_TAGS_RAW="$(eyeD3 "$DIR_MUSIC/$song" 2>/dev/null)"
    META_TAGS_ARTIST="$(echo "$META_TAGS_RAW" | grep -m 1 "artist" | awk '{print $2}')"
    META_TAGS_TITLE="$(echo "$META_TAGS_RAW" | grep "title" | awk '{$1=""}1')"
    META_TAGS_GENRE="$(echo "$META_TAGS_RAW" | grep "genre" | awk '{print $3}')"
    META_TAGS="\033[0;32mA: \033[0;33m$META_TAGS_ARTIST \033[0;32mT: \033[0;33m$META_TAGS_TITLE \033[0;32mG: \033[0;33m$META_TAGS_GENRE"
    
    read -p "$(printf "\033[0;32mCanción \033[1;33m$song 
$META_TAGS
\033[1;31my[Si] n[No] s[Salir] \033[0m") -> " meta
    case "$meta" in
        [Nn]* )
            clear
            continue
            ;;
        [Ss]* )
            exit 0
            ;;
        [Yy]* )
            clear
            echo "\033[0;32mCanción \033[1;33m$song
$META_TAGS"
            ;;
        *)
            echo "Opción $confirm no valida"
            exit 1
            ;;
    esac

    if [ "$ARTIST" = "todos" ]
    then
        TMP_ARTIST="$(echo $song | awk -F'-' '{print $1}' | sed -e "s/_/ /g")"
        read -p "$(printf "\033[0;32mArtista \033[1;33m$TMP_ARTIST \033[0;32m?:\033[0m") -> " artist
    else
        read -p "$(printf "\033[0;32mArtista \033[1;33m$ARTIST \033[0;32m?:\033[0m") -> " artist
    fi

    if [ ! -z "$artist" ]
    then
        META_ARTIST="$artist"
    else
        if [ ! -z "$TMP_ARTIST" ]
        then
            META_ARTIST="$TMP_ARTIST"
        else
            META_ARTIST="$ARTIST"
        fi
    fi
    META_ARTIST="$(echo "$META_ARTIST" | sed -e "s/_/ /g" | awk '{print tolower($0)}' | sed -e "s/\b\(.\)/\u\1/g" | sed -e "s/ /_/g")"
   
    if [ -z "$TITLE" ]
    then
        TMP_TITLE="$(echo $song | awk -F'-' '{print $2}' | awk -F'.' '{print $1}' | sed -e "s/_/ /g")"
        read -p "$(printf "\033[0;32mTitulo \033[1;33m$TMP_TITLE \033[0;32m?:\033[0m") -> " title
    else
        read -p "$(printf "\033[0;32mTitulo \033[1;33m$TITLE \033[0;32m?:\033[0m") -> " title
    fi

    if [ ! -z "$title" ]
    then
        META_TITLE="$title"
    else
        if [ ! -z "$TMP_TITLE" ]
        then
            META_TITLE="$TMP_TITLE"
        elif [ -z "$TITLE" ]
        then
            notify-send -u critical "No se añadió un titulo para la metadata" && exit 1
        else
            META_TITLE="$TITLE"
        fi
    fi
    META_TITLE="$(echo "$META_TITLE" | sed -e "s/_/ /g" | awk '{print tolower($0)}' | sed -e "s/\b\(.\)/\u\1/g")"
     
    read -p "$(printf "\033[0;32mGenero \033[1;33m$GENDER \033[0;32m?:\033[0m") -> " gender
    if [ ! -z "$gender" ]
    then
        META_GENDER="$gender"
    else
        if [ -z "$GENDER" ]
        then
            notify-send -u critical "No se añadió un genero para la metadata" && exit 1
        else
            META_GENDER="$GENDER"
        fi
    fi
    META_GENDER=$(echo "$META_GENDER" | awk '{print tolower($0)}' | sed -e "s/\b\(.\)/\u\1/g")
     
    read -p "$(printf "\033[0;36mConfirmar: 
\033[0;36m----\033[0;32mCanción \033[1;33m$song
\033[0;36m----\033[0;32mArtista \033[1;33m$META_ARTIST
\033[0;36m----\033[0;32mTitulo \033[1;33m$META_TITLE  
\033[0;36m----\033[0;32mGenero \033[1;33m$META_GENDER 
\033[1;31my[Si] n[No] s[Salir] \033[0m") -> " confirm
    case "$confirm" in
        [Yy]* )
            eyeD3 --remove-all "$DIR_MUSIC/$song" 
            eyeD3 -a "$META_ARTIST" -t "$META_TITLE" -G "$META_GENDER" "$DIR_MUSIC/$song" 
            mpc update
            clear
            ;;
        [Nn]* )
            clear
            ;;
        [Ss]* )
            exit 0
            ;;
        * )
            echo "Opción $confirm no valida"
            exit 1
            ;;
    esac
done 3<<LIST
$SONGS
LIST
