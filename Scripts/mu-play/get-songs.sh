#!/bin/sh

# Listamos todas las canciones de nuestro directorio
for song_path in $DIR_MUSIC/*
do
    SONGS="${SONGS}$(basename "$song_path")\n"
done

# Elegimos el tipo de filtro y filtramos
if [ ! "$ARTIST" = "todos" ]
then
    read -p "Filtrar: $(printf "\033[0;32ma[Artista] e[Escribir] n[Sin filtro] s[Salir]\033[0m") -> " filtro
else
    read -p "Filtrar: $(printf "\033[0;32mn[Sin filtro] e[Escribir] s[Salir]\033[0m") -> " filtro
fi

case $filtro in
    [Aa]* )
        SONGS="$(echo $SONGS | grep -i "$ARTIST")"
        
        if [ "$ARTIST" = "todos" ]
        then
            notify-send "No se ingresó ningún artista"
        fi
        ;;
    [Ee]* )
        read -p "$(printf "\033[0;32mDefinir Filtro:\033[0m") -> " my_filter

        SONGS="$(echo $SONGS | grep -i "$my_filter")"
        ;;
    [nN]* )
            SONGS="$(echo $SONGS)"
        ;;
    [Ss]* )
        exit 0
        ;;
    * )
        echo "Opción \"$filtro\" no valida"
        exit 0
        ;;
esac

if [ -z "$SONGS" ]
then
    notify-send -u critical "0 canciones con \"$OPTARG\" como filtro" && exit 1
fi
