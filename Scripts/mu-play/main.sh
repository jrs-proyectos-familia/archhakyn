#!/bin/sh

# Autor: Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
# Script: my-play Script que contiene funcionalidades básicas y esenciales para manejar las canciones y audios

# IMPORTANTE: La utilización o alteración de este script queda bajo tu responsabilidad, el autor del script no se hace responsable de daños o mal uso que provoque dicho script

# ____   ____ ____  ___ ____ _____
#/ ___| / ___|  _ \|_ _|  _ \_   _|
#\___ \| |   | |_) || || |_) || |
# ___) | |___|  _ < | ||  __/ | |
#|____/ \____|_| \_\___|_|    |_|
#

# Variables globales "var.sh"
. "$(dirname $0)/var.sh"

USAGE="$(basename "$0") [-h] -- Script que contiene una lista de tareas básicas para el tratamiento de canciones y audios

Default:
    mpd-play -h

Use:
    mpd-play [Atributos] [Action]

Opciones: 
    -h    [help]            Muestra las opciones a usar en el script
    [Atributos]======================================================================
    -a    [artista]         Nombre del artista. Usos(Metadata, Filtro (Fake, filtra pero en el archivo y no en las metadatas), Renombramiento). Default: todos
    -g    [genero]          Genero de la canción. Usos(Metadata)
    -t    [title]           Titulo de la canción. Usos(Metadata, Renombramiento)
    [Action (1 por ejecución)]=======================================================
    -R    [rename song]     Renombra todas las canciones del directorio de musica
    -M    [add metadata]    Añade la metadata a las canciones
    -D *  [descargar]       Descargar audio de YT
"

check_args () {
    if [ -z "$1" ]
    then
        echo "No se permiten argumentos vacios";
        exit 1
    fi

    if [ $(echo $1 | cut -c1-1) = "-" ] 
    then
        echo "No se permiten pasar flags como argumentos $1";
        exit 1
    fi
}

while getopts ':hRMD:a:t:g:' option; do
    case "$option" in
        h)
            echo "$USAGE"
            exit 0
            ;;
        a)
            check_args "$OPTARG"

            ARTIST="$OPTARG"
            ;;
        t)
            check_args "$OPTARG"
            
            TITLE="$(echo "$OPTARG" | sed -e "s/ /_/g")"
            ;;
        g)
            check_args "$OPTARG"
            GENDER="$OPTARG"
            ;;
        R)
            ACTION="rename"
            ;;
        M)
            ACTION="meta"
            ;;
        D)
            URL_SONG="$OPTARG"
            ACTION="download"
            ;;
        \?)
            echo -e "Flag -$OPTARG no disponible.\nPrueba en usar: hs-system -h"
            exit 1
            ;;
        :)
            echo -e "Falta de argumentos para el flag -$OPTARG.\nPrueba en usar: hs-system -h"
            exit 1
            ;;
    esac
done
shift $((OPTIND-1)) # Reiniciamos el valor del OPTIND

if [ -n "$1" ]
then
   notify-send -u critical -i error "Acción no permitida... revisa la ayuda" && echo "$USAGE" && exit 1
else
    if [ ! -z "$ACTION" ]
    then
        if [ "$ACTION" = "rename" ]
        then
            . "$(dirname $0)/rename-directory.sh"
        elif [ "$ACTION" = "meta" ]
        then
            . "$(dirname $0)/add-metadata.sh"
        elif [ "$ACTION" = "download" ]
        then
            . "$(dirname $0)/download.sh"
        fi
    else
        echo "$USAGE"
    fi
    
    exit 0
fi
