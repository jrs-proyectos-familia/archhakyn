#!/bin/sh

. "$(dirname $0)/get-songs.sh"

while IFS= read -r song <&3;
do
    TITLE_SONG="$(echo "$song" | awk -F'.' '{$NF=""}1')"
    EXTENSION=$(echo "$song" | rev | cut -c 1-3 | rev)
    read -p "Renombrar: $(printf "\033[1;31m[$ARTIST] \033[1;33m$TITLE_SONG \033[0;32my[Si] n[No] s[Salir]\033[0m") -> " yns
    case $yns in 
        [Yy]* )
            if [ ! -z "$TITLE" ]
            then
                read -p "$(printf "\033[0;32mDefault \033[1;33m$TITLE \033[0;32m:\033[0m") -> " name_song
                
                [ -z "$name_song" ] && name_song="$TITLE"
            else
                read -p "$(printf "\033[0;32mNombre:\033[0m") -> " name_song
            fi

            if [ -z "$name_song" ]
            then
                notify-send -u critical "No se añadió un nombre a la canción" && exit 1
            else
                name_song="$(echo "$name_song" | sed -e "s/ /_/g")"
            fi
            NEW_NAME="$ARTIST-$name_song.$EXTENSION"
            
            mv "$DIR_MUSIC/$song" "$DIR_MUSIC/$NEW_NAME" && notify-send "Canción $NEW_NAME renombrado correctamente"
            mpc update
            ;;
        [Nn]* )
            clear
            ;;
        [Ss]* )
            exit 0
            ;;
        * )
            echo "Opción $yns no valida"
            exit 1
            ;;
    esac
done 3<<LIST
$SONGS
LIST

