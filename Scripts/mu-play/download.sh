#!/bin/sh

if [ "$ARTIST" = "todos" ]
then
    read -p "$(printf "\033[0;32mArtista?:\033[0m") -> " artist
    ARTIST="$artist"
fi
RAW_ARTIST="$(echo "$ARTIST" | sed -e "s/_/ /g" | awk '{print tolower($0)}')"
META_ARTIST="$(echo "$RAW_ARTIST" | sed -e "s/\b\(.\)/\u\1/g" | sed -e "s/ /_/g")"
SONG_ARTIST="$(echo "$RAW_ARTIST" | sed -e "s/ /_/g")"

if [ -z "$TITLE" ]
then
    read -p "$(printf "\033[0;32mTitulo?:\033[0m") -> " title
    TITLE="$title"
fi
RAW_TITLE="$(echo "$TITLE" | sed -e "s/_/ /g" | awk '{print tolower($0)}')"
META_TITLE="$(echo "$RAW_TITLE" | sed -e "s/\b\(.\)/\u\1/g")"
SONG_TITLE="$(echo "$RAW_TITLE" | sed -e "s/ /_/g")"

if [ -z "$GENDER" ]
then
    read -p "$(printf "\033[0;32mGenero?:\033[0m") -> " gender
    GENDER="$gender"
fi
META_GENDER=$(echo "$GENDER" | awk '{print tolower($0)}' | sed -e "s/\b\(.\)/\u\1/g")

NEW_SONG="$SONG_ARTIST-$SONG_TITLE.mp3"

youtube-dl -o "$DIR_MUSIC/$NEW_SONG" --extract-audio --audio-format mp3 $URL_SONG &&
eyeD3 --remove-all "$DIR_MUSIC/$NEW_SONG" &&
eyeD3 -a "$META_ARTIST" -t "$META_TITLE" -G "$META_GENDER" "$DIR_MUSIC/$NEW_SONG" &&
mpc update
