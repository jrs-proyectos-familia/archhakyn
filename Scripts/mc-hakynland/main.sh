#!/bin/sh

#  __  __    _    ___ _   _ 
# |  \/  |  / \  |_ _| \ | |
# | |\/| | / _ \  | ||  \| |
# | |  | |/ ___ \ | || |\  |
# |_|  |_/_/   \_\___|_| \_|
#                           

# AUTHOR: Ing. Joaquín Reyes Sánchez [Hakyn Seyer]
#  EMAIL: joaquin.seyer21@gmail.com
#   DATE: dom-03-mayo-2020
# SCRIPT: mc-hakynland
#  NOTES: Script que contiene funcionalidades básicas para configurar un servidor skyblock desde dmenu y demás
# NOTICE: La utilización o alteración de este script queda bajo tu responsabilidad, el autor del script no se hace responsable de daños o mal uso que provoque dicho script al entorno en el que se ejecute

########## [ GLOBALES ] ##########
# "var.sh": Varibles globales del proyecto
. "$(dirname $0)/var.sh"
# "func.sh": Funciones globales del proyecto
. "$(dirname $0)/func.sh"
########## [ GLOBALES ] ##########





USAGE="$(basename "$0") [-h] -- Script que contiene una lista de tareas básicas para la configuración del servidor skyblock

Default:
    mc-skyblock -h

Use:
    mc-skyblock [Tareas]

Opciones: 
    -h    [help]            Muestra las opciones a usar en el script
    [Tareas]======================================================================
    -c    [challenge]       Administrar los desafios del skyblock [Crear, Editar, Borrar]
"

while getopts ':hc' option; do
    case "$option" in
        h)
            echo "$USAGE"
            exit 0
            ;;
        c)
            . "$(dirname $0)/challenges.sh"

            # Apagar el servicio de MariaDB
            echo "Cerrando proceso de MariaDB"
            sudo systemctl stop mariadb.service

            echo "Proceso Terminado... Vuelve Pronto"

            exit 0
            ;;
        \?)
            echo -e "Flag -$OPTARG no disponible.\nPrueba en usar: hs-system -h"
            exit 1
            ;;
        :)
            echo -e "Falta de argumentos para el flag -$OPTARG.\nPrueba en usar: hs-system -h"
            exit 1
            ;;
    esac
done
shift $((OPTIND-1)) # Reiniciamos el valor del OPTIND

if [ -n "$1" ]
then
   notify-send -u critical -i error "Acción no permitida... revisa la ayuda" && echo "$USAGE" && exit 1
else
    echo "$USAGE"
    exit 0
fi
