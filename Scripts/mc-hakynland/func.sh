#!/bin/sh

#  _____ _   _ _   _  ____ _____ ___ ___  _   _ ____  
# |  ___| | | | \ | |/ ___|_   _|_ _/ _ \| \ | / ___| 
# | |_  | | | |  \| | |     | |  | | | | |  \| \___ \ 
# |  _| | |_| | |\  | |___  | |  | | |_| | |\  |___) |
# |_|    \___/|_| \_|\____| |_| |___\___/|_| \_|____/ 
#                                                     

# AUTHOR: Ing. Joaquín Reyes Sánchez [Hakyn Seyer]
#  EMAIL: joaquin.seyer21@gmail.com
#   DATE: dom-03-mayo-2020
# SCRIPT: mc-hakynland
#  NOTES: Script que guarda todas las funciones globales disponibles para usar en cualquier momento del proyecto
# NOTICE: La utilización o alteración de este script queda bajo tu responsabilidad, el autor del script no se hace responsable de daños o mal uso que provoque dicho script al entorno en el que se ejecute


####################################################
# STATE: DEPRECATED
# DESCRIPTION: Función que crea un directorio en la cache destinada al proyecto; y solo si existe dicha ruta del directorio
####################################################
create_folder() {
    # Revisar si existe el directorio del script
    [ ! -d "$DIR_CACHE" ] && mkdir "$DIR_CACHE"
}

####################################################
# STATE: DEPRECATED
# DESCRIPTION: Función que carga la lista de materiales tomada desde la documentación oficial de Bukkit Spigot
####################################################
Charge_Materials() {
    create_folder

    # Descargando la lista de materiales de Bukkit si no existe el archivo
    [ ! -f "$MATERIALS_CACHE" ] && echo "Descargando Materiales" && curl -s https://hub.spigotmc.org/javadocs/bukkit/org/bukkit/Material.html >"$MATERIALS_CACHE"

    # Actualizando la lista de materiales de Bukkit si ha pasado al menos 1 día
    [ ! "$(stat -c %y "$MATERIALS_CACHE" | cut -d' ' -f1)" = "$(date '+%Y-%m-%d')" ] && echo "Descargando Materiales" && curl -s https://hub.spigotmc.org/javadocs/bukkit/org/bukkit/Material.html >"$MATERIALS_CACHE"

    # Consiguiendo la lista de los materiales
    MATERIALS="$(cat "$MATERIALS_CACHE")"

    # Lanzar error si no se logró conseguir la información de los materiales
    [ -z "$MATERIALS" ] && notify-send -u critical "Hubo un problema al intentar conseguir la lista de materiales bukkit" && exit 1

    # Fixeando la lista Materials
    MATERIALS="$(echo "$MATERIALS" | grep class=\"colFirst\" | grep class=\"memberNameLink\" | awk -F'>' '{print $5}' | sed 's/<\/a//g')"

    # Lanzar error si no se logró fixear la información de los materiales
    [ -z "$MATERIALS" ] && notify-send -u critical "Hubo un problema al intentar fixear la lista de Materiales de bukkit" && exit 1
}

####################################################
# STATE: WORKING
# DESCRIPTION: Función que lista el grupo de materiales deseados desde la libreria java de Bukkit
# PARAMS: 1) [String] Tipo de materiales a listar (solid-blocks-entities)
# RETURN: [String] Lista de materiales Bukkit
####################################################
Get_Materials_Bukkit() {
    # Comprobamos si existe el Jar Bukkit
    [ ! -f "$BUKKIT_JAR" ] && notify-send -u critical "No exite el archivo jar $BUKKIT_JAR. Necesario para obtenerner la lista de materiales" && exit 1

    # Filtramos el tipo de material a listar
    case "$1" in
        "solid")
            java -jar "$BUKKIT_JAR" -m && exit 0
            ;;
        "blocks")
            java -jar "$BUKKIT_JAR" --materials-blocks && exit 0
            ;;
        "entities")
            java -jar "$BUKKIT_JAR" -e && exit 0
            ;;
        *)
            notify-send -u critical "No existe la opción $1 en materiales Bukkit Spigot" && exit 1
            ;;
    esac
}

####################################################
# STATE: TODO
# DESCRIPTION: Función que carga la lista de materiales de Bukkit desde la librería java oficial
# RETURN: [String] Lista de materiales Bukkit
####################################################
Charge_Materials_API() {
    # Comprobar si existe el jar
    [ ! -f "$BUKKIT_JAR" ] && notify-send -u critical "No exite el archivo jar $BUKKIT_JAR. Necesario para obtenerner la lista de materiales" && exit 1

    java -jar ~/apps/hakynsland/Bukkit-A1.0.0.jar -m

    #MATERIALS="$(java -jar ~/apps/hakynsland/Bukkit-A1.0.0.jar -m)"

    # Lanzar error si no se logró fixear la información de los materiales
    #[ -z "$MATERIALS" ] && notify-send -u critical "Hubo un problema al intentar fixear la lista de Materiales de bukkit" && exit 1
}


Charge_Materials_API_Blocks() {
    # Comprobar si existe el jar
    [ ! -f "$BUKKIT_JAR" ] && notify-send -u critical "No exite el archivo jar $BUKKIT_JAR. Necesario para obtenerner la lista de materiales" && exit 1

    java -jar ~/apps/hakynsland/Bukkit-A1.0.0.jar --materials-blocks
}

Charge_Entities_API() {
    # Comprobar si existe el jar
    [ ! -f "$BUKKIT_JAR" ] && notify-send -u critical "No exite el archivo jar $BUKKIT_JAR. Necesario para obtenerner la lista de materiales" && exit 1

    java -jar ~/apps/hakynsland/Bukkit-A1.0.0.jar -e
}

Charge_Challenges() {
    create_folder

    # Estado de MariaDB
    local Estado_Maria="$(sudo systemctl status mariadb.service)"

    # Habilitando el servicio de MariaDB si es necesario
    [ "$(echo "$Estado_Maria" | grep Active | awk '{print $2}')" = "inactive" ] && echo "Habilitando proceso de MariaDB" && sudo systemctl start mariadb.service && echo "MariaDB arrancando"

    # Obteniendo la información de la base de datos
    if [ -z "$DB_PASS" ]; then
        # Sin Password
        CHALLENGES=$(mysql -s -N -h "$DB_HOST" -u "$DB_USER" -D "$USE_BENTOBOX" -e "Select json from \`$TABLE_BENTOBOX_CHALLENGES\`;")
    else
        # Con Password
        CHALLENGES=$(mysql -s -N -h "$DB_HOST" -u "$DB_USER" -p "$DB_PASS" -D "$USE_BENTOBOX" -e "Select json from \`$TABLE_BENTOBOX_CHALLENGES\`;")
    fi

    # Lanzar error si no se cargaron los desafios
    [ -z "$CHALLENGES" ] && notify-send -u critical "Hubo un problema con la base de datos. No se pudo conseguir los desafíos" && exit 1

    # Fixeando el JSON de los challenges
    CHALLENGES="[ $(echo $CHALLENGES | sed "s/\\\n/$MATERIALS_BREAKLINE/g" | sed 's/^} {/},{/g') ]"

    # Lanzar error si no se logró fixear correctamente el JSON
    [ -z "$CHALLENGES" ] && notify-send -u critical "Hubo un problema al intentar fixear el JSON de los desafíos" && exit 1

    # Convertir a formato JSON
    echo "$CHALLENGES" | jq '.' >"$CHALLENGES_FILE"
}

Charge_Challenges_IDS() {
    # Consiguiendo el Json de los Challenges
    local Number_Challenges="$(cat "$CHALLENGES_FILE" | jq '. | length')"
    local Unique_Id=""

    for I in $(seq 0 $Number_Challenges); do
        Unique_Id="$(cat "$CHALLENGES_FILE" | jq ".[$I].uniqueId")"
        Unique_Id="$(echo "$Unique_Id" | sed 's/\"//g')"

        if [ ! "$Unique_Id" = "null" ]; then
            if [ -z "$CHALLENGES_IDS" ]; then
                CHALLENGES_IDS="$Unique_Id"
            else
                CHALLENGES_IDS="${CHALLENGES_IDS}\n$Unique_Id"
            fi
        fi
    done
}

Charge_ItemStack_Names() {
    for I in $(seq 0 $(($(cat "$ITEMSTACK_FILE" | yq 'keys | length') - 1))); do
        local Current_Key="$(cat "$ITEMSTACK_FILE" | yq 'keys' | yq ".[$I]")"

        local Child_Keys="$(cat "$ITEMSTACK_FILE" | yq ".$Current_Key" | yq "keys")"

        for J in $(seq 0 $(($(echo "$Child_Keys" | yq '. | length') - 1))); do
            if [ -z "$ITEMSTACK_NAMES" ]; then
                ITEMSTACK_NAMES="$Current_Key$ITEMSTACK_SEPARATOR$(echo "$Child_Keys" | yq ".[$J]")"
            else
                ITEMSTACK_NAMES="${ITEMSTACK_NAMES}\n$Current_Key$ITEMSTACK_SEPARATOR$(echo "$Child_Keys" | yq ".[$J]")"
            fi
        done
    done
}

Get_Content_Integer() {
    #1) Field
    #2) Desc

    local Normal_Value=0
    local Suggested_Value=0

    while true; do
        if [ "$Field" = "repeatExperienceReward" ]; then
            Normal_Value="$(echo "$Json_Challenge" | jq '.rewardExperience')"

            Suggested_Value="$(node -e "console.log(Math.ceil($Normal_Value * $CHALLENGES_REDUCE_REWARD_EXP))")"

            Content="$(echo "$Suggested_Value\nCancelar" | dmenu -i -p "Campo \"$1\"")"
        elif [ "$Field" = "repeatMoneyReward" ]; then
            Normal_Value="$(echo "$Json_Challenge" | jq '.rewardMoney')"

            Suggested_Value="$(node -e "console.log(Math.ceil($Normal_Value * $CHALLENGES_REDUCE_REWARD_MONEY))")"

            Content="$(echo "$Suggested_Value\nCancelar" | dmenu -i -p "Campo \"$1\"")"
        else
            Content="$(echo "1\nCancelar" | dmenu -i -p "Campo \"$1\"")"
        fi

        # Salir del bucle si ha seleccionado Cancelar o presionado Esc
        [ -z "$Content" ] || [ "$Content" = "Cancelar" ] && Content="" && return 2

        # Validar si el contenido es un número
        case "$Content" in
        '' | *[!0-9]*) ;;

        *)
            break
            ;;
        esac
    done

    return 1
}

Get_Content_Option() {
    #1) Field
    #2) Desc
    #3) Options

    while true; do
        Content="$(echo "$3\nCancelar" | dmenu -i -p "Campo \"$1\"")"

        # Salir del bucle si ha seleccionado Cancelar o presionado Esc
        [ -z "$Content" ] || [ "$Content" = "Cancelar" ] && Content="" && return 2

        # Validar si se ha elegido una opción clave
        Exit_Content="$(echo "$3" | while IFS= read -r Opt_Content; do
            [ "$Content" = "$Opt_Content" ] && echo "Salir" && break
        done)"

        [ "$Exit_Content" = "Salir" ] && break
    done

    return 1
}

Get_Content() {
    # 1) Field
    # 2) Desc
    # 3) Color
    # 4) Treat (lower, upper, array)
    # 5) Val (noEmpty)

    # -> 1 (Correcto)
    # -> 2 (Cancelacion o Esc)
    # -> 3 (Error de validación)
    if [ "$5" = "noEmpty" ]; then
        Content="$(echo "Cancelar" | dmenu -i -p "Campo \"$1\"")"
    else
        Content="$(echo "Cancelar\nVacio" | dmenu -i -p "Campo \"$1\"")"
    fi

    # Salir del bucle si ha seleccionado Cancelar o presionado Esc
    [ -z "$Content" ] || [ "$Content" = "Cancelar" ] && Content="" && return 2

    # Opción Vacio
    if [ "$Content" = "Vacio" ]; then
        if [ "$4" = "array" ]; then
            Content="[]"
        else
            Content=""
        fi

        return 1
    fi

    # Tratamiento
    case "$4" in
    "lower")
        Content="$(echo "$Content" | awk '{print tolower($0)}')"
        ;;
    "upper")
        Content="$(echo "$Content" | awk '{print toupper($0)}')"
        ;;
    "array")
        local Number_Characters_By_Line=70
        local Number_Words_By_Line=12
        local Array="[]"
        #Content="Buenos días mi nombre es Joaquin Reyes Sanchez y soy el creador del servidor HakynLand... espero que sea de su grata diversión y espero verlos en persona ahí mismo. De mi parte muchisimas gracias por creer en mi."

        if [ "${#Content}" -gt $Number_Characters_By_Line ]; then
            # Formador de Lineas
            local Current_Pos_Content=1
            local Pos_Line=0

            while [ $Current_Pos_Content -lt ${#Content} ]; do
                local Content_Line="$3"
                local Stop_Line="true"
                local Current_Pos_Line=0

                while [ "$Stop_Line" = "true" ]; do
                    local Content_Word=""
                    local Stop_Word="true"

                    while [ "$Stop_Word" = "true" ]; do
                        local Current_Char="$(printf '%s\n' "$Content" | cut -c$Current_Pos_Content)"

                        Content_Word="${Content_Word}$Current_Char"
                        Current_Pos_Content=$(($Current_Pos_Content + 1))

                        [ -z "$Current_Char" ] && Stop_Word="false"
                        [ "$Current_Char" = " " ] && Stop_Word="false"
                    done

                    Content_Line="${Content_Line}$Content_Word"
                    Current_Pos_Line=$(($Current_Pos_Line + 1))

                    [ "$Current_Pos_Line" = "$Number_Words_By_Line" ] && Stop_Line="false"
                done

                Array="$(echo "$Array" | jq ".[$Pos_Line] = \"$Content_Line\"")"
                Pos_Line=$(($Pos_Line + 1))
            done
        else
            # 1 Sola Linea
            Array="$(echo "$Array" | jq ".[0] = \"$Color$Content\"")"
        fi

        Content="$Array"
        return 1
        ;;
    esac

    # Tratamiento Especial Campos
    case "$1" in
    "uniqueId")
        Content="BSkyBlock_$(echo "$Content" | sed 's/ /_/g')"
        ;;
    esac

    # Validaciones
    case "$5" in
    "noEmpty")
        [ -z "$Content" ] && notify-send -u critical "El campo \"$1\" no puede estar vacio" && return 3
        ;;
    esac

    # Añadir Color
    [ -z "$5" ] && Content="$3${Content}"

    return 1
}

Get_Other_Island() {
    # 1) Field
    # 2) Json

    local Main_Json="$2"

    # Campos Vacios
    Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.requiredExperience = 0")"
    Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.takeExperience = false")"
    Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.requiredMoney = 0")"
    Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.takeMoney = false")"
    Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.requiredIslandLevel = 0")"
    Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.requiredPermissions = []")"

    while true; do
        Get_Content_Option "Other Island" "Desafíos extra de la Isla" "Experiencia\nDinero\nNivel Isla"

        # Salir del bucle si ha seleccionado Cancelar o presionado Esc
        [ "$?" -eq 2 ] && break

        case "$Content" in
        "Experiencia")
            Get_Content_Integer "Quantity Experience" "Cantidad de Experiencia"

            # Salir del bucle si ha seleccionado Cancelar o presionado Esc
            [ "$?" -eq 2 ] && break

            Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.requiredExperience = $Content")"

            # Preguntar si tomará la experiencia del jugador
            Get_Content_Option "Remove Experience" "Remover la Experiencia" "Si\nNo"

            if [ "$Content" = "Si" ]; then
                Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.takeExperience = true")"
            else
                Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.takeExperience = false")"
            fi
            ;;
        "Dinero")
            Get_Content_Integer "Quantity Money" "Cantidad de Dinero"

            # Salir del bucle si ha seleccionado Cancelar o presionado Esc
            [ "$?" -eq 2 ] && break

            Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.requiredMoney = $Content")"

            # Preguntar si tomará el dinero del jugador
            Get_Content_Option "Remove Money" "Remover el Dinero" "Si\nNo"

            if [ "$Content" = "Si" ]; then
                Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.takeMoney = true")"
            else
                Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.takeMoney = false")"
            fi
            ;;
        "Nivel Isla")
            Get_Content_Integer "Level Island" "Nivel de la Isla"

            # Salir del bucle si ha seleccionado Cancelar o presionado Esc
            [ "$?" -eq 2 ] && break

            Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.requiredIslandLevel = $Content")"
            ;;
        esac
    done

    echo "$Main_Json"
}

Get_Item_Island() {
    # 1) Field
    # 2) Json

    local Main_Json="$2"

    # Cargamos la lista de materiales
    local Materials_Blocks="$(Charge_Materials_API_Blocks)"
    local Entities_All="$(Charge_Entities_API)"

    # Campos Vacios
    Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.requiredBlocks = {}")"
    Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.removeBlocks = false")"
    Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.requiredEntities = {}")"
    Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.removeEntities = false")"
    Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.searchRadius = 0")"
    Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.requiredPermissions = []")"

    while true; do
        Get_Content_Option "Item Island" "Objetos a buscar en la Isla" "Bloques\nEntidades"

        # Salir del bucle si ha seleccionado Cancelar o presionado Esc
        [ "$?" -eq 2 ] && break

        if [ "$Content" = "Bloques" ]; then
            local Block_Item=""
            local Quantity_Block=""

            # Agregando Bloques
            Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.requiredBlocks = {}")"

            while true; do
                Get_Content_Option "Item Island" "Bloque a rastrear en la Isla" "$Materials_Blocks"

                # Salir del bucle si ha seleccionado Cancelar o presionado Esc
                [ "$?" -eq 2 ] && break

                Block_Item="$Content"

                # Solicitamos la cantidad a guardar
                Get_Content_Integer "Quantity Block \"$Block_Item\"" "Cantidad del Bloque \"$Block_Item\""

                # Salir del bucle si ha seleccionado Cancelar o presionado Esc
                [ "$?" -eq 2 ] && break

                Quantity_Block="$Content"

                Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.requiredBlocks.$Block_Item = $Quantity_Block")"
            done

            # Validar si realmente se agregaron Bloques
            if [ "$(echo "$Main_Json" | jq ".$1.parameters.requiredBlocks" | jq 'keys | length')" -gt 0 ]; then
                # Remover Bloques
                Get_Content_Option "Remove Blocks" "Remover los Bloques" "Si\nNo"

                # Salir del bucle si ha seleccionado Cancelar o presionado Esc
                [ "$?" -eq 2 ] && break

                if [ "$Content" = "Si" ]; then
                    Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.removeBlocks = true")"
                else
                    Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.removeBlocks = false")"
                fi
            fi
        elif [ "$Content" = "Entidades" ]; then
            local Entity_Name=""
            local Quantity_Entity=""

            # Agregando Entidad Vacia
            Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.requiredEntities = {}")"

            while true; do
                Get_Content_Option "Entity Island" "Entidad a rastrear en la Isla" "$Entities_All"

                # Salir del bucle si ha seleccionado Cancelar o presionado Esc
                [ "$?" -eq 2 ] && break

                Entity_Name="$Content"

                # Solicitamos la cantidad de entidades a rastrear
                Get_Content_Integer "Quantity Entity \"$Entity_Name\"" "Cantidad de Entidades \"$Entity_Name\""

                # Salir del bucle si ha seleccionado Cancelar o presionado Esc
                [ "$?" -eq 2 ] && break

                Quantity_Entity="$Content"

                Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.requiredEntities.$Entity_Name = $Quantity_Entity")"
            done
        fi
    done

    # Validar si se agregaron Bloques o Entidades
    if [ "$(echo "$Main_Json" | jq ".$1.parameters.requiredBlocks" | jq 'keys | length')" -gt 0 ] || [ 1 -gt 0 ]; then
        # Radio de Busqueda
        Get_Content_Integer "Radius by searching Objects" "Radio de busqueda de los Objectos"

        # Salir del bucle si ha seleccionado Cancelar o presionado Esc
        [ "$?" -eq 2 ] && break

        Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.searchRadius = $Content")"
    fi

    # Requisito del Permiso
    Main_Json="$(echo "$Main_Json" | jq ".$1.parameters.requiredPermissions = []")"

    echo "$Main_Json"
}

Get_Inventory_Island() {
    # 1) Field
    # 2) Json

    local Main_Json="$2"

    # Cargamos la lista de materiales
    local Materials_All="$(Charge_Materials_API)"

    while true; do
        Get_Content_Option "Type Inventory" "Tipo de Inventario" "Normal\nItemStack"

        # Salir del bucle si ha seleccionado Cancelar o presionado Esc
        [ "$?" -eq 2 ] && break

        case "$Content" in
        "Normal") ;;

        \
            "ItemStack") ;;

        esac

    done

    echo "$Main_Json"
}

Get_Item_Bukkit() {
    # 1) Field
    # 2) Json

    local Main_Json="$2"
    local Counter=0

    # Valores Limpios
    Main_Json="$(echo "$Main_Json" | jq ".$1 = []")"

    while true; do
        # No se mostrará el vacio siempre y cuando el Counter sea mayor a 0
        Get_Content_Option "Item" "Elige una opción" "Normal\nItemStack"

        # Salir del bucle si ha seleccionado Cancelar o presionado Esc
        [ "$?" -eq 2 ] && break

        if [ "$Content" = "Normal" ]; then
            Get_Content_Option "$1" "" "$MATERIALS"

            # Salir del bucle si ha seleccionado Cancelar o presionado Esc
            [ "$?" -eq 2 ] && break

            Item_Content="$MATERIALS_BUKKIT$Content$MATERIALS_BREAKLINE "

            Get_Content_Integer "$1" ""

            # Salir del bucle si ha seleccionado Cancelar o presionado Esc
            [ "$?" -eq 2 ] && break

            # Si es un valor negativo bloquearlo
            [ "$Content" -lt 1 ] && notify-send -u critical "No se admiten valores negativos" && exit 1

            Item_Content="${Item_Content}amount: $Content$MATERIALS_BREAKLINE"

            Main_Json="$(echo "$Main_Json" | jq ".$1[$Counter] = \"$Item_Content\"")"

            Counter="$(($Counter + 1))"
        elif [ "$Content" = "ItemStack" ]; then
            Get_Content_Option "ItemStack" "Elige un item" "$(echo "$ITEMSTACK_NAMES" | awk -F"$ITEMSTACK_SEPARATOR" '{print $2}' | sed 's/"//g')"

            # Salir del bucle si ha seleccionado Cancelar o presionado Esc
            [ "$?" -eq 2 ] && break

            # LO DE ABAJO SE PUEDE SOLUCIONAR CON LA RECURSIVIDAD
            #===    =======================================================================

            local Main_Key="$(echo "$ITEMSTACK_NAMES" | grep "$Content" | awk -F"$ITEMSTACK_SEPARATOR" '{print $1}' | sed 's/"//g')"

            local Item_Object="$(cat "$ITEMSTACK_FILE" | yq ".\"$Main_Key\".\"$Content\".item")"
            local Item_Object_Keys="$(echo "$Item_Object" | yq 'keys_unsorted')"

            Content="is:$ITEMSTACK_SEPARATOR "

            for I in $(seq 0 $(($(echo "$Item_Object_Keys" | yq '. | length') - 1))); do
                local Item_Key="$(echo "$Item_Object_Keys" | yq ".[$I]" | sed 's/"//g')"
                local Item_Content_Key="$(echo "$Item_Object" | yq ".\"$Item_Key\"")"

                if [ "$Item_Key" = "meta" ]; then
                    Content="${Content}$Item_Key:$ITEMSTACK_SEPARATOR "
                    local Item_Object_Child_Keys="$(echo "$Item_Content_Key" | yq 'keys_unsorted')"

                    for J in $(seq 0 $(($(echo "$Item_Object_Child_Keys" | yq '. | length') - 1))); do
                        local Item_Key_Child="$(echo "$Item_Object_Child_Keys" | yq ".[$J]" | sed 's/"//g')"
                        local Item_Content_Child="$(echo "$Item_Content_Keys" | yq ".\"$Item_Key_Child\"")"

                        if [ "$Item_Key_Child" = "enchants" ]; then
                            Content="${Content}$Item_Key_Child:$ITEMSTACK_SEPARATOR "

                            local Item_Key_Child_Keys="$(echo "$Item_Content_Child" | yq 'keys_unsorted')"

                            for K in $(seq 0 $(($(echo "$Item_Key_Child_Keys" | yq '. | length') - 1))); do
                                local I_K="$(echo "$Item_Key_Child_Keys" | yq ".[$K]" | sed 's/"//g')"
                                local I_C="$(echo "$Item_Content_Child" | yq ".\"$I_K\"")"

                                Content="${Content}$I_K: $(echo "$I_C" | sed 's/"//g')$ITEMSTACK_SEPARATOR "
                            done
                        elif [ "$Item_Key_Child" = "lore" ]; then
                            Content="${Content}$Item_Key_Child:$ITEMSTACK_SEPARATOR "

                            for M in $(seq 0 $(($(echo "$Item_Content_Child" | yq '. | length') - 1))); do
                                local I_C="$(echo "$Item_Content_Child" | yq ".[$M]")"

                                Content="${Content}- $(echo "$I_C" | sed 's/"//g')$ITEMSTACK_SEPARATOR "
                            done
                        else
                            Content="${Content}$Item_Key_Child: $(echo "$Item_Content_Child" | sed 's/"//g')$ITEMSTACK_SEPARATOR "
                        fi
                    done
                else
                    Content="${Content}$Item_Key: $(echo "$Item_Content_Key" | sed 's/"//g')$ITEMSTACK_SEPARATOR "
                fi
            done
            #===    =======================================================================
            # Guardamos la copia en la variable
            Item_Content="$Content"

            Get_Content_Integer "$1" ""

            # Salir del bucle si ha seleccionado Cancelar o presionado Esc
            [ "$?" -eq 2 ] && break

            # Si es un valor negativo bloquearlo
            [ "$Content" -lt 1 ] && notify-send -u critical "No se admiten valores negativos" && exit 1

            Item_Content="${Item_Content}amount: $Content$MATERIALS_BREAKLINE"

            Main_Json="$(echo "$Main_Json" | jq ".$1[$Counter] = \"$Item_Content\"")"

            Counter="$(($Counter + 1))"
        fi
    done

    echo "$Main_Json"
}
