#!/bin/sh

Charge_Challenges
Charge_Challenges_IDS

#Charge_Materials_API
#Charge_ItemStack_Names

New_Challenge () {
    # Objecto Json del nuevo desafio
    local Json_Challenge="{}"
    local Field=""
    local Variable=""
    local Color=""
    local Content=""
    local Unique_Content=""

    # Leyendo Template Challenges
    while IFS= read -r line
    do
        Field="$(echo "$line" | awk -F'@' '{print $1}')"
        Variable="$(echo "$line" | awk -F'@' '{print $2}')"
        Color="$(echo "$line" | awk -F'@' '{print $3}')"

        # Rellenar con default cuando no se requiere que el desafío sea repetible
        if [ ! -z "$(echo "$Json_Challenge" | jq '.repeatable')" ]
        then
            if [ "$(echo "$Json_Challenge" | jq '.repeatable')" = "false" ]
            then
                Content=""

                case "$Field" in
                    "repeatRewardText")
                    Json_Challenge="$(echo "$Json_Challenge" | jq ".$Field = \"\"" )"
                    ;;
                "maxTimes" | "repeatExperienceReward" | "repeatMoneyReward") 
                    Json_Challenge="$(echo "$Json_Challenge" | jq ".$Field = 1" )"
                    ;;
                "repeatItemReward" | "repeatRewardCommands")
                    Json_Challenge="$(echo "$Json_Challenge" | jq ".$Field = []" )"
                esac
                
                continue
            fi
        fi
        
        case "$Variable" in
            "Integer")
                [ "$Field" = "order" ] && Json_Challenge="$(echo "$Json_Challenge" | jq ".$Field = -1" )" && continue

                Get_Content_Integer "$Field" ""
                
                # Salir del bucle si ha seleccionado Cancelar o presionado Esc
                [ "$?" -eq 2 ] && break

                
                Json_Challenge="$(echo "$Json_Challenge" | jq ".$Field = $Content" )"
                ;;
            "String")
                if [ "$Field" = "uniqueId" ]
                then
                    while true
                    do
                        Get_Content "$Field" "" "$Color" "lower" "noEmpty"
                        
                        # Salir del bucle si ha seleccionado Cancelar o presionado Esc
                        [ "$?" -eq 2 ] && break
                        
                        Unique_Content="$(echo "$CHALLENGES_IDS" | while IFS= read -r id_challenge
                        do
                            if [ "$Content" = "$id_challenge" ]
                            then
                                echo "Copia"
                                break
                            fi
                        done)"
                    
                        # Salir siempre y cuando se valide que el titulo es único
                        [ -z "$Unique_Content" ] && break

                        notify-send -u critical "El id \"$Content\" ya ha sido asignado a otro desafío"
                    done
               
                    # Enviar atras si no hay un Id Asignado
                    [ -z "$Content" ] && break
                    
                    # Asignación del ID al desafío
                    Json_Challenge="$(echo "$Json_Challenge" | jq ".$Field = \"$Content\"" )"
                else
                    if [ "$Field" = "friendlyName" ] ||
                        [ "$Field" = "rewardText" ]
                    then 
                        Get_Content "$Field" "" "$Color" "" "noEmpty"
                    else
                        Get_Content "$Field" "" "$Color" "" ""
                    fi
                    
                    # Salir del bucle si ha seleccionado Cancelar o presionado Esc
                    [ "$?" -eq 2 ] && break
                    
                    # Asignación del Campo al desafío
                    Json_Challenge="$(echo "$Json_Challenge" | jq ".$Field = \"$Content\"" )"
                fi
                ;;
            "ArrayString")
                    # Aqui caerán todos aquellos atributos tipo Array String
                    if [ "$Field" = "description" ]
                    then
                        Get_Content "$Field" "" "$Color" "array" "noEmpty"
                    else
                        Get_Content "$Field" "" "$Color" "array" ""
                    fi
                    
                    # Salir del bucle si ha seleccionado Cancelar o presionado Esc
                    [ "$?" -eq 2 ] && break
                    
                    # Asignación del Campo al desafío
                    local Size_Array_Content=$(echo "$Content" | jq '. | length')

                    if [ $Size_Array_Content -eq 0 ]
                    then
                        Content=""
                        Json_Challenge="$(echo "$Json_Challenge" | jq ".$Field = []" )"
                    else
                        for I in $(seq 0 $(($Size_Array_Content - 1)))
                        do
                            local Line_Json="$(echo "$Content" | jq ".[$I]")"

                            Json_Challenge="$(echo "$Json_Challenge" | jq ".$Field[$I] = $Line_Json" )"
                        done
                    fi
                ;;
            "Boolean")
                Get_Content_Option "$Field" "" "Si\nNo"
                    
                # Salir del bucle si ha seleccionado Cancelar o presionado Esc
                [ "$?" -eq 2 ] && break

                [ "$Content" = "No" ] && Json_Challenge="$(echo "$Json_Challenge" | jq ".$Field = false" )"
                [ "$Content" = "Si" ] && Json_Challenge="$(echo "$Json_Challenge" | jq ".$Field = true" )"
                ;;
            "Empty")
                # Campo environment
                [ "$Field" = "environment" ] && Json_Challenge="$(echo "$Json_Challenge" | jq ".$Field = []")"
                ;;
            "ChallengeType")
                Get_Content_Option "$Field" "" "INVENTORY\nISLAND\nOTHER"
                
                # Salir del bucle si ha seleccionado Cancelar o presionado Esc
                [ "$?" -eq 2 ] && break

                Json_Challenge="$(echo "$Json_Challenge" | jq ".$Field = \"$Content\"")"
                ;;
            "Material")
                Get_Content_Option "$Field" "" "$MATERIALS"

                # Salir del bucle si ha seleccionado Cancelar o presionado Esc
                [ "$?" -eq 2 ] && break

                Json_Challenge="$(echo "$Json_Challenge" | jq ".$Field = \"$MATERIALS_BUKKIT$Content$MATERIALS_BREAKLINE\"")"
                ;;
            "ArrayItem")
                Json_Challenge="$(Get_Item_Bukkit "$Field" "$Json_Challenge")"
                ;;
            "Requirements")
                # Agregando la clase
                if [ "$(echo "$Json_Challenge" | jq '.challengeType' | sed 's/"//g')" = "INVENTORY" ]
                then
                    Json_Challenge="$(echo "$Json_Challenge" | jq ".$Field.class = \"InventoryRequirements\"")"
                elif [ "$(echo "$Json_Challenge" | jq '.challengeType' | sed 's/"//g')" = "ISLAND" ]
                then
                    Json_Challenge="$(echo "$Json_Challenge" | jq ".$Field.class = \"IslandRequirements\"")"
                elif [ "$(echo "$Json_Challenge" | jq '.challengeType' | sed 's/"//g')" = "OTHER" ]
                then
                    Json_Challenge="$(echo "$Json_Challenge" | jq ".$Field.class = \"OtherRequirements\"")"
                fi

                # Validando si se agregó correctamente la clase
                [ "$(echo "$Json_Challenge" | jq ".$Field.class")" = "null" ] && notify-send -u critical "No se pudo determinar el tipo de desafio a aplicar" && exit 1

                # Separando los requisitos para cumplir el desafío
                case "$(echo "$Json_Challenge" | jq ".$Field.class" | sed 's/"//g')" in
                    "InventoryRequirements")
                        Get_Inventory_Island "$Field" "$Json_Challenge"

                        exit 1

                        # Feature futura: Mover el contenido de aqui a una función con el nombre de Get_Inventory_Island
                        Json_Challenge="$(Get_Item_Bukkit "$Field.parameters.requiredItems" "$Json_Challenge")"
    
                        # Si ha escogido la opción vacio entonces rellenar todos los campos por el valor default
                        if [ "$(echo "$Json_Challenge" | jq ".$Field.parameters.requiredItems | length")" -eq 0 ]
                        then
                            Content="No"
                        else
                            Get_Content_Option "takeItems" "¿Tomar Items?" "Si\nNo"
                        
                            # Salir del bucle si ha seleccionado Cancelar o presionado Esc
                            [ "$?" -eq 2 ] && break
                        fi

                        if [ "$Content" = "Si" ]
                        then
                            Json_Challenge="$(echo "$Json_Challenge" | jq ".$Field.parameters.takeItems = true")"
                        else
                            Json_Challenge="$(echo "$Json_Challenge" | jq ".$Field.parameters.takeItems = false")"
                        fi
                        
                        Json_Challenge="$(echo "$Json_Challenge" | jq ".$Field.parameters.requiredPermissions = []")"
                        ;;
                    "IslandRequirements")
                        Json_Challenge="$(Get_Item_Island "$Field" "$Json_Challenge")"
                        ;;
                    "OtherRequirements")
                        Json_Challenge="$(Get_Other_Island "$Field" "$Json_Challenge")"
                        ;;
                esac

                echo "$Json_Challenge"
                exit 1
                ;;
        esac
    done < "$CHALLENGES_TEMPLATE"


    echo "$Json_Challenge"
}

# Menú inicial en dmenu
while true
do
    Inicial="$(echo "Nuevo Desafío\nEditar Desafío\nEliminar Desafío\nSalir" | dmenu -p "Menú Desafios" -i)"

    # Salir cuando se presione Esc
    [ -z "$Inicial" ] && break

    case "$Inicial" in
        "Nuevo Desafío")
            New_Challenge
            ;;
        "Salir")
                break
            ;;
    esac
done

