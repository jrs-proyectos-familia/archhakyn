#!/bin/sh

#  ____ _     ___  ____    _    _      __     ___    ____  ____
# / ___| |   / _ \| __ )  / \  | |     \ \   / / \  |  _ \/ ___|
#| |  _| |  | | | |  _ \ / _ \ | |      \ \ / / _ \ | |_) \___ \
#| |_| | |__| |_| | |_) / ___ \| |___    \ V / ___ \|  _ < ___) |
# \____|_____\___/|____/_/   \_\_____|    \_/_/   \_\_| \_\____/
#

# Base de datos Mysql-MariaDB
DB_HOST="localhost"
DB_USER="root"
DB_PASS=""

# Archivo Jar de Bukkit
BUKKIT_JAR="$HOME/apps/hakynsland/Bukkit-A1.0.0.jar"

# Bases de datos Bentobox
USE_BENTOBOX="mc_bentobox_hs"
# Tablas de Bentobox
TABLE_BENTOBOX_CHALLENGES="world.bentobox.challenges.database.object.Challenge"

# Folder script del cache
DIR_CACHE="$HOME/.cache/hs-scripts/minecraft/hakynland-skyblock"

# Json Challenges
CHALLENGES=""
CHALLENGES_FILE="$DIR_CACHE/challenges.json"
CHALLENGES_TEMPLATE="$(dirname $0)/challenges-template.txt"
CHALLENGES_IDS=""
CHALLENGES_REDUCE_REWARD_EXP=0.50
CHALLENGES_REDUCE_REWARD_MONEY=0.50

# Materials Bukkit
BUKKIT_BREAKLINE="æ"

MATERIALS_BUKKIT="is:$MATERIALS_BREAKLINE ==: org.bukkit.inventory.ItemStack$MATERIALS_BREAKLINE v: 2230$MATERIALS_BREAKLINE type: "
MATERIALS_CACHE="$DIR_CACHE/materials.txt"
MATERIALS=""

# ItemStack
# DEPRECATED ITEMSTACK_SEPARATOR="æ"
ITEMSTACK_FILE="$HOME/juegos/minecraft/servers/hakynsland/plugins/ItemEdit/database/player-database.yml"
ITEMSTACK_NAMES=""

