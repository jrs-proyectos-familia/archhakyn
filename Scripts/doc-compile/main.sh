#!/bin/bash

# Autor: Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
# Script: doc-compile Script para compilar distintos textos (markdown por ejemplo) a PDF

# IMPORTANTE: La utilización o alteración de este script queda bajo tu responsabilidad, el autor del script no se hace responsable de daños o mal uso que provoque dicho script

# ____   ____ ____  ___ ____ _____
#/ ___| / ___|  _ \|_ _|  _ \_   _|
#\___ \| |   | |_) || || |_) || |
# ___) | |___|  _ < | ||  __/ | |
#|____/ \____|_| \_\___|_|    |_|
#

killall entr

# $1 = Ruta directa del archivo más archivo

FILE_EXTENSION="${1##*/}"
FILE="${FILE_EXTENSION%%.*}"
EXTENSION="${FILE_EXTENSION##*.}"
ROUTE="${1%/*}"

case "$EXTENSION" in
    "rmd")
        echo "$1" | entr pandoc -t html5 -f markdown "$1" -o "$FILE.pdf" | echo "$ROUTE/styles.css" | entr pandoc -t html5 -f markdown "$1" -o "$FILE.pdf"
        ;;
    "pu")
        echo "$1" | entr plantuml "$1"
        ;;
      *)
          notify-send -u critical -i error "No existe una compilación para $1"
        ;;
esac
