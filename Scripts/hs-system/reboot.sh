#!/bin/sh

# Script para reiniciar el sistema

[ "$(echo "No\nSi" | dmenu -i -p "¿Reiniciar el sistema?" )" = "Si" ] && reboot
