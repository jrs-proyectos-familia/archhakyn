Name=Libnda
Exec=/home/hakyn/codigo/scripts/bash/libnda-launch/main.sh i3-rofi-floating
Comment=Libro para guardar cuentas y contraseñas del usuario

Name=Minecraft
Exec=cd /opt/minecraft-launcher && minecraft-launcher 2>/dev/null
Comment=Juego de mundo abierto [SandBox]

Name=VS Code
Exec=/usr/bin/code-oss --no-sandbox --unity-launch
Comment=Editor de código fuente

Name=Transmission
Exec=transmission-gtk
Comment=Programa para manejar torrents

Name=Insomnia
Exec=insomnia
Comment=Aplicación para debugear APIS

Name=Pale Moon
Exec=palemoon
Comment=Navegador web estilo VIM

Name=Guvcview
Exec=guvcview
Comment=Aplicación para lanzar una webcam

Name=Chromium
Exec=/usr/bin/chromium 2>/dev/null
Comment=Navegador web chrome

Name=Krita
Exec=krita 2>/dev/null
Comment=Programa para dibujo digital

Name=Firefox
Exec=firefox
Comment=Navegador web mozilla firefox

Name=FreeCAD
Exec=freecad
Comment=Programa para el diseño asistido por computadora de piezas

Name=SceneBuilder
Exec=/opt/SceneBuilder/SceneBuilder 2>/dev/null
Comment=Aplicación para crear interfaces graficas usando JavaFX

Name=Alacritty
Exec=alacritty
Comment=Emulador de terminal programado en Rust

Name=Flameshot
Exec=flameshot
Comment=Herramienta para dibujar en la pantalla actual

Name=Arandr
Exec=arandr
Comment=Herramienta para configurar multiples pantallas

Name=Audacity
Exec=audacity 2>/dev/null
Comment=Programa para editar audios

Name=Gimp
Exec=gimp-2.10 2>/dev/null
Comment=Programa de manipulación de imagenes

Name=VLC media player
Exec=vlc 2>/dev/null
Comment=Reproductor de multimedia

Name=Zathura
Exec=zathura
Comment=Visualizador de documentos PDF y otros

Name=Dwarf Fortress
Exec=dwarffortress
Comment=Juego de fantasía single-player

Name=Pencil
Exec=/usr/bin/pencil %f
Comment=Aplicación para crear mockups
