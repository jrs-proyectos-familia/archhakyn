#!/bin/sh
# Script para auto bloquear la pantalla

MONITOR_DISPLAY="$(xrandr | grep primary | awk '{print $1}')"

# Si existe un proceso ya existente, entonces lo cerramos
[ $(pgrep xidlehook | wc -l) -gt 0 ] && pkill xidlehook

xidlehook \
    `# Desactivar la acción cuando exista un fullscreen` \
    --not-when-fullscreen \
    `# Desactivar la acción cuando exista una canción o audio` \
    --not-when-audio \
    `# Quitar brillo despues de 180 segundos` \
    --timer 180 \
        "xrandr --output "$MONITOR_DISPLAY" --brightness .1" \
        "xrandr --output "$MONITOR_DISPLAY" --brightness 1" \
    `# 300 segundos despues, activar i3lock` \
    --timer 300 \
        "xrandr --output "$MONITOR_DISPLAY" --brightness 1; i3lock -i /home/hakyn/imagenes/wallpapers/naturaleza/bosque_otonio.png" \
        '' \
    `# 10 minutos despues, suspender el equipo` \
    --timer 600 \
        'systemctl suspend' \
        ''
