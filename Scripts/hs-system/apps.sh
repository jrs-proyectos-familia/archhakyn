#!/bin/sh
# Script para abrir las aplicaciones mas usadas

# Variables globales "var.sh"
. "$(dirname $0)/var.sh"

APPS="$(cat "$APPS_JSON")"
#APPS_LEN="$(echo $APPS | jq -r ". | length")"
APPS_LEN="$(echo "$APPS" | grep -cP '\S')"
APPS_LEN=$((APPS_LEN / 3))

APPS_DMENU=""

line=1
for i in $(seq 1 $APPS_LEN)
do
    APP_CONTENT="$(echo "$APPS" | sed -n "$line, $((line + 2))p")"
    
    APP_NAME=$(echo "$APP_CONTENT" | grep Name | awk -F'=' '{print $2}')
    APP_COMMENT=$(echo "$APP_CONTENT" | grep Comment | awk -F'=' '{print $2}')
    
    APPS_DMENU="${APPS_DMENU}[$line] $APP_NAME ($APP_COMMENT)\n"
    
    line=$((line + 4))
done

# Remuevo lineas vacías
APPS_DMENU=$(echo $APPS_DMENU | sed -r '/^\s*$/d')

APP_SELECT=$(echo "$APPS_DMENU" | dmenu -i -l 5 -p "Abrir App:")

# Si presiona Esc, termina el script
[ -z "$APP_SELECT" ] && exit 0

APP_INDEX=$(echo "$APP_SELECT" | awk -F'[][]' '{print $2}')

# Si no se consigue el index, termina el script con error
[ ! ${APP_INDEX:=0}  -gt 0 ] && notify-send -u critical "No se encontró la aplicación:" "$APP_SELECT" && exit 1

APP_EXEC=$(echo "$APPS" | sed -n "$APP_INDEX, $((APP_INDEX + 2))p" | grep Exec | awk -F'=' '{print $2}')

# Si no se consigue el Exec del APP, termina el script con error
if [ -z "$APP_EXEC" ] || [ "$APP_EXEC" = "null" ]
then
    notify-send -u critical "No se encontró el comando de ejecución para:" "$APP_SELECT" && exit 1
fi

# Inicia la aplicación
eval "$APP_EXEC 2>/dev/null" || notify-send -u critical "No se pudo abrir la aplicación:" "$APP_SELECT"
