#!/bin/sh

# Autor: Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
# Script: hs-system Script que contiene funcionalidades básicas y esenciales para tareas frecuentes dentro del sistema Arch

# IMPORTANTE: La utilización o alteración de este script queda bajo tu responsabilidad, el autor del script no se hace responsable de daños o mal uso que provoque dicho script

# ____   ____ ____  ___ ____ _____
#/ ___| / ___|  _ \|_ _|  _ \_   _|
#\___ \| |   | |_) || || |_) || |
# ___) | |___|  _ < | ||  __/ | |
#|____/ \____|_| \_\___|_|    |_|
#

# Variables globales "var.sh"
. "$(dirname $0)/var.sh"

USAGE="$(basename "$0") [-h] -- Script que contiene una lista de tareas básicas para el sistema Arch y muy usadas por Hakyn Seyer

Default:
    hs-system -h

Tareas: 
    -h    [help]            Muestra las opciones a usar en el script
    -e    [exit shutdown]   Apagar el sistema usando el comando shutdown 
    -r    [reboot]          Reiniciar el sistema
    -a    [apps]            Lanza dmenu con las aplicaciones a abrir
    -A    [open apps.json]  Abre el archivo apps.json para su modificación
    -x    [autolock]        Ejecuta un autolock con el fin de proteger la computadora
"
while getopts ':heaAxr' option; do
    case "$option" in
        h)
            echo "$USAGE"
            exit 0
            ;;
        e)
            . "$(dirname $0)/exit.sh"
            exit 0
            ;;
        r)
            . "$(dirname $0)/reboot.sh"
            exit 0
            ;;
        a)
            . "$(dirname $0)/apps.sh"
            exit 0
            ;;
        A)
            "$EDITOR" "$APPS_JSON"
            exit 0
            ;;
        x)
            . "$(dirname $0)/lock.sh"
            exit 0
            ;;
        \?)
            echo -e "Flag -$OPTARG no disponible.\nPrueba en usar: hs-system -h"
            exit 1
            ;;
        :)
            echo -e "Falta de argumentos para el flag -$OPTARG.\nPrueba en usar: hs-system -h"
            exit 1
            ;;
    esac
done
shift $((OPTIND-1)) # Reiniciamos el valor del OPTIND

if [ -n "$1" ]
then
   notify-send -u critical -i error "Acción no permitida... revisa la ayuda" && echo "$USAGE" && exit 1
else
    echo "$USAGE"
    exit 0
fi

 
