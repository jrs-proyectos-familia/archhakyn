#!/bin/sh
# Script para apagar el sistema

[ "$(echo "No\nSi" | dmenu -i -p "¿Apagar el sistema?")" = "Si" ] && shutdown -h now
