#!/bin/bash

# Autor: Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
# Script: pid-libnda Script que busca el PID ultimo del programa Libnda

# IMPORTANTE: La utilización o alteración de este script queda bajo tu responsabilidad, el autor del script no se hace responsable de daños o mal uso que provoque dicho script

# ____   ____ ____  ___ ____ _____
#/ ___| / ___|  _ \|_ _|  _ \_   _|
#\___ \| |   | |_) || || |_) || |
# ___) | |___|  _ < | ||  __/ | |
#|____/ \____|_| \_\___|_|    |_|
#

# Conseguir el último PID de cada aplicación LIBNDA
#pgrep -n java
ps aux | grep ".*\.jar$" | grep libnda | tail -n 1 | awk '{print $2}'
