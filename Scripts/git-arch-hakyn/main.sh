#!/bin/bash

# Autor: Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
# Script: git-arch-hakyn Script de configuraciónes de sistema arch de Hakyn Seyer

# IMPORTANTE: La utilización o alteración de este script queda bajo tu responsabilidad, el autor del script no se hace responsable de daños o mal uso que provoque dicho script

# ____   ____ ____  ___ ____ _____
#/ ___| / ___|  _ \|_ _|  _ \_   _|
#\___ \| |   | |_) || || |_) || |
# ___) | |___|  _ < | ||  __/ | |
#|____/ \____|_| \_\___|_|    |_|
#

# Modifica la ruta para que apunte al archivo "var.sh"
source $HOME/codigo/scripts/bash/git-arch-hakyn/var.sh


# Contadores de archivos copiados
NEW_FILES=0
NEW_FILES_NAME=""
UPDATED_FILES=0
UPDATED_FILES_NAME=""
REMOVED_FILES=0
REMOVED_FILES_NAME=""

function create_group () {
    # $1 Nombre del Grupo

    if [ ! -z "$1" ]
    then
        if [ ! -d "$BASE_ROUTE/$1" ]
        then
            notify-send -i messagebox_info "Se ha creado el grupo  $1"
            mkdir "$BASE_ROUTE/$1"
        fi
    else
        notify-send -u critical -i error "Es necesario el nómbre del grupo "; exit 1
    fi
}

function generate_note () {
    # $1 Ruta de la nota a guardar
    # $2 Lista de archivos copiados por elemento
    # $3 Nota del elemento a guardar
    # $4 Nombre del grupo
    # $5 Nombre del elemento
    # $6 Numero de archivos por elemento

    local WRITE_NOTE="0"

    if [ "$NEW_FILES" -gt "0" ] || [ "$UPDATED_FILES" -gt "0" ] || [ "$REMOVED_FILES" -gt "0" ]
    then
        if [ "$4" = "Scripts" ]
        then
            local NAME_NOTE="$(echo $1 | awk -F'/' '{print $NF}')"
            local PATH_NOTE="$(echo $1 | sed s/"$NAME_NOTE"/"$5"/g)"

            if [ -d "$PATH_NOTE" ]
            then
                PATH_NOTE="${PATH_NOTE}/$NAME_NOTE"

                # Remover Notas Existentes
                if [ -f "$PATH_NOTE" ]
                then
                    rm "$PATH_NOTE"
                fi

                local WRITE_NOTE="1"
            
            fi
        else
            local PATH_NOTE="$1"
            
            # Remover Notas Existentes
            if [ -f "$PATH_NOTE" ]
            then
                rm "$PATH_NOTE"
            fi

            if [ "$6" -ne "$REMOVED_FILES" ] 
            then
                local WRITE_NOTE="1"
            fi
        fi
    fi

    if [ "$WRITE_NOTE" -eq "1" ]
    then
            echo -e "Grupo: $4\nElemento: $5\nCambio:$(date +"%d-%B-%Y")\n\nArchivos Tratados: $2\n\nArchivos Nuevos:\n$NEW_FILES_NAME\n\nArchivos Actualizados:\n$UPDATED_FILES_NAME\n\nArchivos Removidos:\n$REMOVED_FILES_NAME\n\nNotas:\n $3" >> $PATH_NOTE
    fi

}

function generate_files () {
    # $1 Ruta del archivo original
    # $2 Ruta copia de destino
    # $3 Nombre del archivo copiado
    # $4 Ruta de la nota del elemento copiado
    # $5 Nota simple del elemento copiado
    # $6 Estado del tratamiento
    
    # Remover archivos Existentes
    if [ -f "$2" ] || [ -d "$2" ]
    then
        rm -r "$2"
    fi

    cp -r "$1" "$2"
    echo "$6 $3"
}

function generate_commit () {
    git --git-dir="$BASE_ROUTE/.git" --work-tree="$BASE_ROUTE" add --all &&
    git --git-dir="$BASE_ROUTE/.git" --work-tree="$BASE_ROUTE" commit -m "$GIT_MESSAGE $(date +"%d-%B-%Y")" &&
    notify-send -i messagebox_info "Commit creado "
}

function upload_gitlab () {
    git --git-dir="$BASE_ROUTE/.git" --work-tree="$BASE_ROUTE" push "$GIT_REMOTE" master &&
    notify-send -i messagebox_info "Actualizado en GitLab "
}

function info_commit () {
    git --git-dir="$BASE_ROUTE/.git" --work-tree="$BASE_ROUTE" log
}

function remove_commit () {
    git --git-dir="$BASE_ROUTE/.git" --work-tree="$BASE_ROUTE" reset --hard HEAD~1
}

function status_git () {
    git --git-dir="$BASE_ROUTE/.git" --work-tree="$BASE_ROUTE" status
}

function remove_element () {
    # $1 NAME_GROUP
    # $2 NAME_ELEMENT
    # $3 CURRENT_FILE_ELEMENT
    # $4 ALIAS_CURRENT_FILE
    # $5 RAW_FILE

    local FILENAME=$(echo $3 | awk -F'/' '{print $NF}' )
    local ELEMENT=$(echo "$2" | awk '{print toupper($0)}')

    if [ "$1" = "Scripts" ]
    then
        local DESTINATION_PATH="$BASE_ROUTE/$1/$FILENAME"
    elif [ ! "$4" = "-" ]
    then
        local ALIAS=$(echo "$4" | awk '{print toupper($0)}')
        local DESTINATION_PATH="$BASE_ROUTE/$1/$ELEMENT-$ALIAS-$FILENAME"
    else
        local DESTINATION_PATH="$BASE_ROUTE/$1/$ELEMENT-$FILENAME"
    fi

    if [ -f "$DESTINATION_PATH" ] 
    then
        local RAW_FILE_FIX="${5//\//\\/}"
        local REMOVE_FROM_FILES_ROUTE=$(sed -i "/$RAW_FILE_FIX/d" "$FILES_ROUTE")
        
        if [ -z "$REMOVE_FROM_FILES_ROUTE" ]
        then
            rm "$DESTINATION_PATH"
        
            echo "REMOVIDO...... $FILENAME"
            
            REMOVED_FILES=$((REMOVED_FILES + 1))
            REMOVED_FILES_NAME="${REMOVED_FILES_NAME}$5\n"
        fi
    elif [ -d "$DESTINATION_PATH" ]
    then
        local RAW_FILE_FIX="${5//\//\\/}"
        local LINE_MATCH="$(grep -n "$RAW_FILE_FIX" "$FILES_ROUTE" | awk -F':' '{print $1}')"
        # sed -i LINEA_INICIAL , LINEA_FINAL d ARCHIVO
        local REMOVE_FROM_FILES_ROUTE=$(sed -i "$((LINE_MATCH - 3)), $((LINE_MATCH + 1)) d" "$FILES_ROUTE")

        if [ -z "$REMOVE_FROM_FILES_ROUTE" ]
        then
            rm -r "$DESTINATION_PATH"

            echo "REMOVIDO...... $FILENAME"

            REMOVED_FILES=$((REMOVED_FILES + 1))
            REMOVED_FILES_NAME="${REMOVED_FILES_NAME}$5\n"
        fi
    else
        notify-send -u critical -i error "No se encuentra el archivo : $DESTINATION_PATH"; exit 1
    fi
}

function new_update () {
    # $1 CURRENT_FILE_ELEMENT
    # $2 NAME_GROUP
    # $3 NAME_ELEMENT
    # $4 NOTES_DESTINATION_PATH 
    # $5 NOTES_ELEMENT
    # $6 ALIAS_CURRENT_FILE
    # $7 RAW_CURRENT_FILE_ELEMENT

    # Nombre del archivo
    local FILENAME=$(echo $1 | awk -F'/' '{print $NF}' )
    
    if [ "$2" = "Scripts" ]
    then
        # Ruta final (con nombre final del archivo) del destino del archivo
        local DESTINATION_PATH="$BASE_ROUTE/$2/$FILENAME"
    else
        local ELEMENT=$(echo "$3" | awk '{print toupper($0)}')
        local ALIAS=$(echo "$6" | awk '{print toupper($0)}')
        
        if [ ! -z "$6" ] && [ ! "$6" = "-" ]
        then
            # Ruta final (con nombre final del archivo) del destino del archivo
            local DESTINATION_PATH="$BASE_ROUTE/$2/$ELEMENT-$ALIAS-$FILENAME"
        else
            # Ruta final (con nombre final del archivo) del destino del archivo
            local DESTINATION_PATH="$BASE_ROUTE/$2/$ELEMENT-$FILENAME"
        fi
    fi
    
    if [ -f "$DESTINATION_PATH" ] || [ -d "$DESTINATION_PATH" ]
    then
        # Archivo a Actualizar
                       
        if [ -d "$DESTINATION_PATH" ]
        then
            local NAME_NOTE="$(echo $4 | awk -F'/' '{print $NF}')"
            local DIFF_FILES=$(diff -rq -x "*$NAME_NOTE" "$1" "$DESTINATION_PATH")
        else
            # Cambios entre archivo original y archivo de destino
            local DIFF_FILES=$(diff -rq "$1" "$DESTINATION_PATH")
        fi
        
        if [ "$DIFF_FILES" != "" ]
        then
            generate_files "$1" "$DESTINATION_PATH" "$FILENAME" "$4" "$5" "ACTUALIZADO..."
                    
            UPDATED_FILES=$((UPDATED_FILES + 1))
            UPDATED_FILES_NAME="${UPDATED_FILES_NAME}$7\n"
        fi

    else
        # Archivo Nuevo
        generate_files "$1" "$DESTINATION_PATH" "$FILENAME" "$4" "$5" "NUEVO........."
                   
        NEW_FILES=$((NEW_FILES + 1))
        NEW_FILES_NAME="${NEW_FILES_NAME}$7\n"
    fi
}

function main () {
    #Lectura del archivo json
    local CONTENT_JSON=$(cat "$FILES_ROUTE")

    # Numero de grupos creados
    local NUM_GROUPS=$(echo "$CONTENT_JSON" | jq '. | length')

    for (( group=0; group<$NUM_GROUPS; group++ ))
    do
        # Nombre del grupo
        local NAME_GROUP=$(echo "$CONTENT_JSON" | jq -r ".[$group].Group")
        
        # Creación de la carpeta del grupo
        create_group $NAME_GROUP
    
        # Numero de Elementos del grupo
        local NUM_ELEMENTS=$(echo "$CONTENT_JSON" | jq ".[$group].Elements | length")
        
        for (( element=0; element<$NUM_ELEMENTS; element++ ))
        do
            # Nombre del elemento
            local NAME_ELEMENT=$(echo "$CONTENT_JSON" | jq -r ".[$group].Elements[$element].Name")
            # Notas del elemento
            local NOTES_ELEMENT=$(echo "$CONTENT_JSON" | jq -r ".[$group].Elements[$element].Notes")
            # Ruta final de las notas por elemento
            local NOTES_DESTINATION_PATH="$BASE_ROUTE/$NAME_GROUP/$(echo "$NAME_ELEMENT" | awk '{print toupper($0)}')-NOTAS.txt"
            # Manejo del grupo Scripts
            if [ "$NAME_GROUP" = "Scripts" ]
            then
                # Archivos del elemento
                local FILES_ELEMENT="[ $(echo "$CONTENT_JSON" | jq ".[$group].Elements[$element].Folder") ]"
            else
                # Archivos del elemento
                local FILES_ELEMENT=$(echo "$CONTENT_JSON" | jq -r ".[$group].Elements[$element].Files")
            fi

            # Reseteo de variables de notas
            NEW_FILES=0
            UPDATED_FILES=0
            REMOVED_FILES=0
            NEW_FILES_NAME=""
            UPDATED_FILES_NAME=""
            REMOVED_FILES_NAME=""
            
            # Numero de archivos para el elemento
            local NUM_FILES_ELEMENT=$(echo "$FILES_ELEMENT" | jq '. | length')
            
            for (( file=0; file<$NUM_FILES_ELEMENT; file++ ))
            do
                # Archivo a guardar y Alias
                local RAW_CURRENT_FILE_ELEMENT=$(echo "$FILES_ELEMENT" | jq -r ".[$file]")

                local CURRENT_FILE_ELEMENT=$(echo $RAW_CURRENT_FILE_ELEMENT | awk -F' ' '{print $1}')
                local ALIAS_CURRENT_FILE=$(echo $RAW_CURRENT_FILE_ELEMENT | awk -F' ' '{print $2}')
                local ACTION_CURRENT_FILE=$(echo $RAW_CURRENT_FILE_ELEMENT | awk -F' ' '{print $3}')

                if [ "$ACTION_CURRENT_FILE" = "REMOVE" ]
                then
                    remove_element "$NAME_GROUP" "$NAME_ELEMENT" "$CURRENT_FILE_ELEMENT" "$ALIAS_CURRENT_FILE" "$RAW_CURRENT_FILE_ELEMENT"
                else
                    if [ -f "$CURRENT_FILE_ELEMENT" ]
                    then
                        # Son archivos a guardar
                        new_update "$CURRENT_FILE_ELEMENT" "$NAME_GROUP" "$NAME_ELEMENT" "$NOTES_DESTINATION_PATH" "$NOTES_ELEMENT" "$ALIAS_CURRENT_FILE" "$RAW_CURRENT_FILE_ELEMENT"
                    elif [ "$NAME_GROUP" = "Scripts" ] && [ -d "$CURRENT_FILE_ELEMENT" ]
                    then
                        # Es un Script y es una carpeta la que se va a copiar
                        new_update "$CURRENT_FILE_ELEMENT" "$NAME_GROUP" "$NAME_ELEMENT" "$NOTES_DESTINATION_PATH" "$NOTES_ELEMENT" "$ALIAS_CURRENT_FILE" "$RAW_CURRENT_FILE_ELEMENT"
                    else
                        notify-send -u critical -i error "No existe el archivo  $RAW_CURRENT_FILE_ELEMENT" && exit 1
                    fi
                fi
                
            done
            
            # Generación del archivo de notas al completar el ciclo de copia por elemento
            generate_note "$NOTES_DESTINATION_PATH" "$FILES_ELEMENT" "$NOTES_ELEMENT" "$NAME_GROUP" "$NAME_ELEMENT" "$NUM_FILES_ELEMENT"

        done
    done
   
    notify-send -i messagebox_info -t 5000 "Se ha completado la tarea... Gracias por esperar :)"
}

# Validación y menu de opciones de entrada
USAGE="$(basename "$0") [-h] -- Script para guardar las configuraciones del sistema $TITLE_MENU y posteriormente si lo deseas generar un git y guardarlo en gitlab

Default:
    git-arch-hakyn -b

Opciones:
    -h   [help]              Muestra las opciones a usar en el script
    -b   [basic]             Copiará los archivos especificados del archivo de files (usar -s) a la ruta destino (usar -d)
    -m   [medium]            Copiará los archivos y posteriormente creará el commit git deseado
    -c   [complete]          Copiará los archivos, creará el commit git y posteriormente lo subirá a gitlab
    -g   [git]               Crear el commit de los archivos guardados
     -i   [git log]           Mostrar la información de los commits creados
     -I   [git status]        Muestra el estado de git del los archivos
     -r   [git reset]         Remueve el último commit creado (Posibles errores si ya ha sido actualizado el repositorio gitlab)
    -l   [gitlab]            Actualizar el repositorio de gitlab de los archivos ya monitoreados por git (No toma en cuenta el crear commit)
     -L   [git-gitlab]        Crear el commit y actualizar el repositorio de gitlab
    -s   [store]             Muestra la ruta del archivo json de configuraciones de archivos a guardar
     -S   [open store]        Abre el archivo de configuraciones a guardar (Abrirá en $EDITOR)
    -d   [destination]       Muestra la ruta de destino de los archivos copiados y monitoreados por git
     -D   [go destination]    Redirige a la ruta de destino de los archivos copiados
"

while getopts ':hbmcgiIrlLsSdD' option; do
    case "$option" in
        h)
            echo "$USAGE"
            exit 0
            ;;
        b)
            main
            
            exit 0
            ;;
        m)
            main 

            if [ $NEW_FILES -gt 0 ] || [ $UPDATED_FILES -gt 0 ]
            then
                generate_commit
            fi
            
            exit 0
            ;;
        c)
            main
            
            if [ $NEW_FILES -gt 0 ] || [ $UPDATED_FILES -gt 0 ]
            then
                generate_commit && upload_gitlab
            fi

            exit 0
            ;;
        g)
            generate_commit

            exit 0
            ;;
        i)
            info_commit

            exit 0
            ;;
        I)
            status_git

            exit 0
            ;;
        r)
            remove_commit

            exit 0
            ;;
        l)
            upload_gitlab

            exit 0
            ;;
        L)
            generate_commit && upload_gitlab

            exit 0
            ;;
        s)
            echo "$FILES_ROUTE"
            
            exit 0
            ;;
        S)
            if [ -z "$EDITOR" ]
            then
                echo -e "Es necesario crear una variable de entorno EDITOR con el commando de ejecución para un editor de textos en particular.\nEjemplo:\nexport EDITOR="vim""
                exit 1
            fi

            $EDITOR "$FILES_ROUTE"

            exit 0
            ;;
        d)
            echo "$BASE_ROUTE"
            
            exit 0
            ;;
        D)
            if [ -z "$TERMINAL" ]
            then
                echo -e "Es necesario crear una variable de entorno TERMINAL con el commando de ejecución de tu terminal para abrir en una ruta especifica.\nEjemplo:\nexport TERMINAL="alacritty --working-directory""
                exit 1
            fi

            $TERMINAL "$BASE_ROUTE" &
            
            exit 0
            ;;
        \?)
            echo -e "Flag -$OPTARG no disponible.\nPrueba en usar: git-arch-hakyn -h"
            exit 1
            ;;
        :)
            echo -e "Falta de argumentos para el flag -$OPTARG.\nPrueba en usar: git-arch-hakyn -h"
            exit 1
            ;;
    esac
done
shift $((OPTIND-1)) # Reiniciamos el valor del OPTIND

if [ -n "$1" ]
then
   notify-send -u critical -i error "Acción no permitida... revisa la ayuda" && echo "$USAGE" && exit 1
else
   main
fi

