#!/bin/bash

#  ____ _     ___  ____    _    _      __     ___    ____  ____
# / ___| |   / _ \| __ )  / \  | |     \ \   / / \  |  _ \/ ___|
#| |  _| |  | | | |  _ \ / _ \ | |      \ \ / / _ \ | |_) \___ \
#| |_| | |__| |_| | |_) / ___ \| |___    \ V / ___ \|  _ < ___) |
# \____|_____\___/|____/_/   \_\_____|    \_/_/   \_\_| \_\____/
#

# Titulo del Menu Script
TITLE_MENU="Arch Linux de Hakyn Seyer"
# Author
# Ruta directa de la carpeta a alojar el output del script
BASE_ROUTE="$HOME/codigo/arch/archHakyn"
# Ruta directa del archivo destinado a cargar los archivos a guardar en git
FILES_ROUTE="$HOME/codigo/scripts/bash/git-arch-hakyn/files.json"
# Mensaje Personalizado del Git Commit
GIT_MESSAGE="Arch Hakyn | Actualización de archivos del sistema Hakyn"
# Nombre del git remote para subir a GitLab (Default origin)... Se ve con git remote -v
GIT_REMOTE="arch"

