#!/bin/bash

# Autor: Joaquin Reyes Sanchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>
# Script: libnda-launch Script que inicia alguna opción de grabación del escritorio y audio del microfono activo

# IMPORTANTE: La utilización o alteración de este script queda bajo tu responsabilidad, el autor del script no se hace responsable de daños o mal uso que provoque dicho script

# ____   ____ ____  ___ ____ _____
#/ ___| / ___|  _ \|_ _|  _ \_   _|
#\___ \| |   | |_) || || |_) || |
# ___) | |___|  _ < | ||  __/ | |
#|____/ \____|_| \_\___|_|    |_|
#

# Modifica la ruta para que apunte al archivo "var.sh"
source $HOME/codigo/scripts/bash/ff-record/var.sh

function notify () {
  # $1 - Tipo de grabación
  # $2 - Mensaje a mostrar

  local ICON=""
  local MENSAJE=""
  
  if [ "$1" = "${LIST_RECORD[0]}" ] || [ "$1" = "${LIST_RECORD[1]}" ]
  then
      ICON="media-record"
  elif [ "$1" = "${LIST_RECORD[2]}" ]
  then
      ICON="audio-recorder"
  elif [ "$1" = "${LIST_RECORD[3]}" ]
  then
      ICON="simplescreenrecorder"
  fi
  
  if [ "$ICON" != "" ]
  then
    COMMAND+="notify-send -i $ICON \"$2\""
  else
    COMMAND=""
  fi
}

function createFolder () {
  # $1 - Nombre del archivo
  # $2 - Ruta especial para guardar la grabación
  
  if [ ! -d "$2/${1// /-}" ] 
  then
    COMMAND+="mkdir -p "$2"/"${1// /-}""
  fi
}

function record () {
  # $1 - Nombre del archivo
  # $2 - Tipo de grabación
  # $3 - Ruta de grabación
 
  local SIZE="$(xdpyinfo | grep dimensions | awk '{print $2}')"
  #local SIZE=1280x720

  case "$2" in
    "${LIST_RECORD[0]}" | "${LIST_RECORD[1]}")
      COMMAND+="ffmpeg \
        -f x11grab \
        -s $SIZE \
        -r 25 \
        -i :0.0"

      if [ "$2" = "${LIST_RECORD[1]}" ]
      then
        COMMAND+="\
            -f alsa \
            -ac 2 \
            -i pulse \
            -acodec aac \
            -strict experimental"
      fi
      
      COMMAND+=" \
        -qscale 5 \
        -f segment \
        -strftime 1 \
        -segment_time 20:00 \
        -segment_format mp4 \
        $3/$1.$2.%d-%m-%Y_%H-%M-%S.mp4"
      
      # Por si se desea partir el video en varios segmentos de 60 segundos
#     COMMAND+=" \
#       -qscale 5 \
#       -f segment \
#       -strftime 1 \
#       -segment_time 60 \
#       -segment_format mp4 \
#       $3/$1.$2.%d-%m-%Y_%H-%M-%S.mp4"
        ;;

    "${LIST_RECORD[2]}")
        COMMAND+="ffmpeg \
          -f alsa \
          -i pulse \
          -f segment \
          -strftime 1 \
          -segment_time 20:00 \
          -segment_format mp3 \
          $3/$1.$2.%d-%m-%Y_%H-%M-%S.mp3"
        ;;
     "${LIST_RECORD[3]}")
         COMMAND+="ffmpeg \
            -f x11grab \
            -s $SIZE \
            -r 1 \
            -i :0.0 \
            -vframes 1 \
            -qscale 5 \
            -strftime 1 \
            $3/$1.$2.%d-%m-%Y_%H-%M-%S.png"
        ;;
      *)
        COMMAND=""
        ;;
   esac
  }

function buildingRecord () {
  # $1 - Tipo de grabación
  # $2 - Nombre del archivo
  # $3 - Ruta de grabación
  # $4 - Activar Folder
  # $5 - Activar Notificaciones

  local NEXT=true
  local ROUTE="$3"

  # CREACION DEL FOLDER CONTENEDOR
  if [ "$4" = "SI" ]
  then
    createFolder "$2" "$3"
    ROUTE+="/${2// /-}"
    if [ "$COMMAND" != "" ]
    then
      COMMAND+=" && "
    fi
  fi

  # LANZAR NOTIFICACIÓN INICIAL (OPCIONAL)
  if [ "$5" = "SI" ]
  then
    notify "$1" "Iniciando grabación"

    if [ "$COMMAND" != "" ]
    then
      if [ "$1" != "${LIST_RECORD[3]}" ]
      then
        #COMMAND+=" && sleep 4 && "
	     COMMAND+=" &&  ( \
	        echo \"0\"; \
	        echo \"# Iniciando en 5\" ; sleep 1; \
	        echo \"20\"; \
	        echo \"# Iniciando en 4\" ; sleep 1; \
	        echo \"40\"; \
	        echo \"# Iniciando en 3\" ; sleep 1; \
	        echo \"60\"; \
	        echo \"# Iniciando en 2\" ; sleep 1; \
	        echo \"80\"; \
	        echo \"# Iniciando en 1\" ; sleep 1; \
	        echo \"100\" \
	        ) | \
	        zenity --progress \
	        --title=\"Grabación Iniciando\" \
	        --percentage=0 \
	        --auto-close \
	        --no-cancel && "
      else
         COMMAND+=" && "
      fi
    else
      NEXT=false
    fi
  fi
  
  # CREACION DE LA GRABACIÓN
  if [ "$NEXT" = true ]
  then
      record "${2// /-}" "$1" "$ROUTE"

    if [ "$COMMAND" != "" ]
    then
      COMMAND+=" && "
    else
      NEXT=false
    fi
  fi

  # CREACION DE LA NOTIFICACIÓN FINAL (OBLIGATORIA)
  if [ "$NEXT" = true ]
  then
    notify "$1" "Grabación Terminada"
  fi

  if [ "$COMMAND" != "" ]
  then
      # Guardado de información en la memoría
      sed -i 's/##TIME##/'$(date +%s)'/g' $FILE_MEMORY
      sed -i 's/##TYPE##/'"$1"'/g' $FILE_MEMORY
      sed -i 's/##FILE##/'"$2"'/g' $FILE_MEMORY
      sed -i 's/##ROUTE##/'"${ROUTE////\\/}"'/g' $FILE_MEMORY
      sed -i 's/##FOLDER##/'"$4"'/g' $FILE_MEMORY
      sed -i 's/##NOTIFY##/'"$5"'/g' $FILE_MEMORY
    
      # Guardado del aviso HTML sobre un preset disponible a continuar
      sed -i 's/##TYPE##/'"$1"'/' $FILE_HTML_CONTINUE 
      sed -i 's/##FILE##/'"$2"'/' $FILE_HTML_CONTINUE 
      sed -i 's/##ROUTE##/'"${ROUTE////\\/}"'/' $FILE_HTML_CONTINUE 
      sed -i 's/##FOLDER##/'"$4"'/' $FILE_HTML_CONTINUE 
      sed -i 's/##NOTIFY##/'"$5"'/' $FILE_HTML_CONTINUE 
     
      eval $COMMAND
  else
    notify-send -u critical -i error "No se pudo generar la grabación"
  fi
}

function getPreset () {
  # $1 - Tipo de grabación
  # $2 - Nombre del archivo
  # $3 - Nombre del preset seleccionado

  local PRESET_TREAT="$(echo ${3// /-})"
  local PRESET="$(cat "$FILE_PRESETS" | grep "$PRESET_TREAT")"

  local PRESET_ROUTE="$(echo $PRESET | awk -F'|' '{print $3}')"
  local PRESET_FOLDER="$(echo $PRESET | awk -F'|' '{print $4}')"
  local PRESET_NOTIFICATIONS="$(echo $PRESET | awk -F'|' '{print $5}')"

  local PRESET_TYPE=$1
  if [ "$1" = "" ]
  then
    PRESET_TYPE="$(echo $PRESET | awk -F'|' '{print $2}')"
  fi

  if [ -d "$HOME/$PRESET_ROUTE" ]
  then
    buildingRecord "$PRESET_TYPE" "$2" "$HOME/$PRESET_ROUTE" "$PRESET_FOLDER" "$PRESET_NOTIFICATIONS"
  else
    notify-send -u critical -i error "No existe el directorio $HOME/$PRESET_ROUTE"
  fi
}

function getDefault () {
  # $1 - Tipo de grabación
  # $2 - Nombre del archivo

  local ROUTE=""
  local NOTIFY="SI"
  
  case "$1" in
    "${LIST_RECORD[0]}" | "${LIST_RECORD[1]}")
        ROUTE="$HOME/videos/capturas"
        ;;
    "${LIST_RECORD[2]}")
        ROUTE="$HOME/audios/capturas"
        ;;
    "${LIST_RECORD[3]}")
        ROUTE="$HOME/imagenes/capturas"
        NOTIFY="NO"
        ;;
  esac

  if [ "$ROUTE" != "" ]
  then 
    buildingRecord "$1" "$2" "$ROUTE" NO  "$NOTIFY"
  else
    notify-send -u critical -i error "No existe el directorio default para guardar las grabaciones"
  fi
}

function prepareRecord () {
  # $1 - Tipo de grabación
  # $2 - Nombre del archivo
  # $3 - Nombre del preset seleccionado

  local ROUTE # Ruta de destino de la grabación
  local FOLDER # Especifica si se debe de crear una carpeta contenedora de la grabación
  local NOTIFICATIONS # Especifica si se debe de lanzar las notificaciones
  
  if [ "$3" != "default" ]
  then
      getPreset "$1" "$2" "$3"
  else
      getDefault "$1" "$2"
  fi
}

function launchFormPreset () {
  # $1 - Tipo de grabación
  # $2 - Nombre del archivo a grabar

  local PRESET_ONLY="$(zenity --forms --add-entry="Nombre del preset" --add-entry="Ruta de las grabaciones $HOME/" --add-combo="¿Crear carpeta contenedora?" --combo-values="SI|NO" --add-combo="¿Activar Notificaciones?" --combo-values="SI|NO")" 
  
  # Al de abajo es para que funcione en i3
  #local PRESET="$(zenity --forms --add-entry="Nombre del preset" --add-entry="Ruta de las grabaciones $HOME/" --add-combo="¿Crear carpeta contenedora?" --combo-values="SI|NO" --add-combo="¿Activar Notificaciones?" --combo-values="SI|NO" & $HOME/codigo/scripts/bash/i3-open-apps $(pgrep -n zenity) floating && echo ">")"   
  # PRESET_ONLY="$(echo "$PRESET" | paste -sd " " | awk -F'>' '{print $2}')"

  if [ "$PRESET_ONLY" != "" ]
  then
    local PRESET_NAME=$(echo $PRESET_ONLY | awk -F'|' '{print $1}')
    local PRESET_ROUTE=$(echo $PRESET_ONLY | awk -F'|' '{print $2}')
    local PRESET_FOLDER=$(echo $PRESET_ONLY | awk -F'|' '{print $3}')
    local PRESET_NOTIFICATIONS=$(echo $PRESET_ONLY | awk -F'|' '{print $4}')
 
    if [ "$PRESET_NAME" != "" ] && [ "$PRESET_ROUTE" != "" ] && [ "$PRESET_FOLDER" != "" ] && [ "$PRESET_NOTIFICATIONS" != "" ]
    then
      echo "${PRESET_NAME// /-}|$1|$PRESET_ROUTE|$PRESET_FOLDER|$PRESET_NOTIFICATIONS" >> $FILE_PRESETS

      if zenity --question --text="¿Usar ahora este preset?" --ellipsize --default-cancel
      then
        prepareRecord "$1" "$2" "$PRESET_NAME"
      else
        init
      fi
    else
      init
    fi

  else
    init
  fi
}

function launchName () {
  # $1 - Tipo de grabación
  # $2 - Preset seleccionado

  local NAME_FILE # Nombre del archivo a grabar

  NAME_FILE="$(zenity --entry --text="Nombre de la grabación")"

  if [ "$NAME_FILE" != "" ]
  then
    if [ "$2" != "" ]
    then
      prepareRecord "$1" "$NAME_FILE" "$2"
    else
      if zenity --question --text="¿Guardar este preset?" --ellipsize --default-cancel
      then
        launchFormPreset "$1" "$NAME_FILE"
      else
        prepareRecord "$1" "$NAME_FILE" default        
      fi
    fi
  else
    notify-send -u critical -i error "Falta el  título de la grabación"
  fi
}

function launchRecord () {
  local RECORD # Tipo de grabación
  
  RECORD="$(zenity --list --text="¿Qué vamos a grabar?" --column="Opciones de grabación" "${LIST_RECORD[@]}")"

  if [ "$RECORD" != "" ]
  then
    launchName $RECORD    
  else
    notify-send -u critical -i error "Es necesario definir una opción de grabación"
  fi
}

function resetRecord () {
   # $1 TIME
   # $2 TYPE
   # $3 FILE
   # $4 ROUTE
   # $5 FOLDER
   # $6 NOTIFY
   # $7 Mensaje para la notificación de limpieza

   # Reseteando la memoria
   sed -i '0,/'"$1"'/s/'"$1"'/##TIME##/' $FILE_MEMORY
   sed -i '0,/'"$2"'/s/'"$2"'/##TYPE##/' $FILE_MEMORY
   sed -i '0,/'"$3"'/s/'"$3"'/##FILE##/' $FILE_MEMORY
   sed -i '0,/'"${4////\\/}"'/s/'"${4////\\/}"'/##ROUTE##/' $FILE_MEMORY
   sed -i '0,/'"$5"'/s/'"$5"'/##FOLDER##/' $FILE_MEMORY
   sed -i '0,/'"$6"'/s/'"$6"'/##NOTIFY##/' $FILE_MEMORY

   # Reseteando el archivo HTML de continuar grabación
   sed -i '0,/'"$2"'/s/'"$2"'/##TYPE##/' $FILE_HTML_CONTINUE
   sed -i '0,/'"$3"'/s/'"$3"'/##FILE##/' $FILE_HTML_CONTINUE
   sed -i '0,/'"${4////\\/}"'/s/'"${4////\\/}"'/##ROUTE##/' $FILE_HTML_CONTINUE
   sed -i '0,/'"$5"'/s/'"$5"'/##FOLDER##/' $FILE_HTML_CONTINUE
   sed -i '0,/'"$6"'/s/'"$6"'/##NOTIFY##/' $FILE_HTML_CONTINUE   


   notify-send -i messagebox_info "$7" "Preset: $2"
   init
}

function killRecord () {
   # $1 Tipo de eliminación

   if [[ "$1" -gt 0 ]]
   then
       if kill "$1"
       then
         notify-send -i messagebox_info "Se ha detenido la grabación" "PID: $1"
       fi
   elif [ "$1" = "all" ]
   then
      if killall ffmpeg
      then
         notify-send -i messagebox_info "Se han detenido todas la grabaciones"
      fi
   elif [ "$1" = "last" ]
   then
      if [[ "$PID_FFMPEG" -gt 0 ]]
      then
         if kill "$PID_FFMPEG"
         then
            notify-send -i messagebox_info "Se ha detenido la última grabación" "PID: $PID_FFMPEG"
         fi
      fi
   fi
}

function init () { 
  local MSG_NEW_RECORD="**** Nueva Grabación ****" # Mensaje para iniciar una nueva grabación
  local MY_PRESETS=$(cat "$FILE_PRESETS"| awk -F'|' '{print $1}')
  local MY_PRESETS_TREAT=()
  local PRESET_RECORD
  local CURRENT_TIME=$(date +%s)
  local DIFF_TIME
  local MINUTES_SPEND
  local SECONDS_SPEND
  local TIME_MS_SPEND

  for p in $MY_PRESETS
  do
    MY_PRESETS_TREAT+=($p)
  done
  
  if [ -z "$1" ]
  then
    if [ ! "${#MY_PRESETS_TREAT[@]}" -eq 0 ]
    then  
       PRESET_RECORD="$(zenity --list --text="¿Usar un preset o nueva grabación?" --column="Mis Presets" "$MSG_NEW_RECORD" "${MY_PRESETS_TREAT[@]//-/ }")"
    
      if [ "$PRESET_RECORD" = "$MSG_NEW_RECORD" ]
      then
        launchRecord
      else
        if [ "$PRESET_RECORD" != "" ]
        then
          launchName "" "$PRESET_RECORD"
        fi
      fi
    else
      launchRecord 
    fi
  else
    case "$1" in
        "desktop")
            getDefault "${LIST_RECORD[3]}" "hs"
            ;;
        "memory")
            local TIME="$(cat "$FILE_MEMORY" | grep time= | awk -F'=' '{print $2}')"
            local TYPE="$(cat "$FILE_MEMORY" | grep type= | awk -F'=' '{print $2}')"
            local FILE="$(cat "$FILE_MEMORY" | grep file= | awk -F'=' '{print $2}')"
            local ROUTE="$(cat "$FILE_MEMORY" | grep route= | awk -F'=' '{print $2}')"
            local FOLDER="$(cat "$FILE_MEMORY" | grep folder= | awk -F'=' '{print $2}')"
            local NOTIFY="$(cat "$FILE_MEMORY" | grep notifications= | awk -F'=' '{print $2}')"

            if [ "$TIME" != "##TIME##" ]
            then
               DIFF_TIME="$(($CURRENT_TIME - $TIME))" 
               MINUTES_SPEND=$(($DIFF_TIME / 60)) 
               SECONDS_SPEND=$(($DIFF_TIME % 60)) #EN UN FUTURO QUIZÁS SE USE
               TIME_MS_SPEND="$MINUTES_SPEND"

               if [ "$TIME_MS_SPEND" -le "$TIME_MS_ALLOWED" ]
               then
                  if  zenity --text-info --filename="$FILE_HTML_CONTINUE" --html
                  then
                     #echo "$TIME" "$TYPE" "$FILE" "$ROUTE" "$FOLDER" "$NOTIFY"
                     if [ "$FOLDER" = "SI" ]
                     then
                         #local ROUTE_2="$(sed 's/'."$FILE"$'//' <<< "$ROUTE")"
                         buildingRecord "$TYPE" "$FILE" "$ROUTE" "NO" "$NOTIFY"
                     else
                         buildingRecord "$TYPE" "$FILE" "$ROUTE" "$FOLDER" "$NOTIFY"
                     fi
                     
                  else
                      resetRecord "$TIME" "$TYPE" "$FILE" "$ROUTE" "$FOLDER" "$NOTIFY" "Se ha eliminado la memoria del preset anterior"
                  fi
               else
                   resetRecord "$TIME" "$TYPE" "$FILE" "$ROUTE" "$FOLDER" "$NOTIFY" "Ha culminado el tiempo de espera para continuar con la grabación"
               fi
            else
               init            
            fi
            ;;
        *)
          notify-send -u critical -i error "No hay soporte para el preset $1"
          ;;
    esac
  fi
}


# Variables de teclado
# $1 - Acceder a una grabación especifica (desktop ó memory)

if [[ "$PID_FFMPEG" -gt 0 ]]
then
    notify-send -i messagebox_info "Existen grabaciones activas" "PID: $PID_FFMPEG"
else 
init "$1"
fi

