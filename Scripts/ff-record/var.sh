#!/bin/bash

#  ____ _     ___  ____    _    _      __     ___    ____  ____
# / ___| |   / _ \| __ )  / \  | |     \ \   / / \  |  _ \/ ___|
#| |  _| |  | | | |  _ \ / _ \ | |      \ \ / / _ \ | |_) \___ \
#| |_| | |__| |_| | |_) / ___ \| |___    \ V / ___ \|  _ < ___) |
# \____|_____\___/|____/_/   \_\_____|    \_/_/   \_\_| \_\____/
#

# Construye el comando completo para realizar la grabación
COMMAND="" 
# Lista de opciones de grabación
LIST_RECORD=(
  "video"
  "video-audio"
  "audio"
  "escritorio"
)
# Delay para mantener guardada la ultima preset de configuración usada
TIME_MS_ALLOWED="46"
# Archivo de almacenamiento de las presets creados
FILE_PRESETS="$HOME/codigo/scripts/bash/ff-record/ff-presets.txt" 
# Archivo que almacena el último preset usado
FILE_MEMORY="$HOME/codigo/scripts/bash/ff-record/ff-lastRecord.txt"
# Archivo que guarda el diseño HTML para el apartado "continuar grabación"
FILE_HTML_CONTINUE="$HOME/codigo/scripts/bash/ff-record/ff-continueRecord.html" 
# Variable que almancena el último PID ffmpeg
PID_FFMPEG="$(pgrep -n ffmpeg)"
