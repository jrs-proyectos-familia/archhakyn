#!/bin/bash

# Variables de teclado
# $1 - Eliminar proceso de ffmpeg dependiendo de una opcion (all, last, #PID)

PID_FFMPEG="$(pgrep -n ffmpeg)"

function killRecord () {
	# $1 Tipo de eliminación
	 
	if [[ "$1" -gt 0 ]]
	then
	   if kill "$1"
	    then
	      notify-send -i messagebox_info "Se ha detenido la grabación" "PID: $1"
	    fi
	elif [ "$1" = "all" ]
	then
	   if killall ffmpeg
	   then
	   notify-send -i messagebox_info "Se han detenido todas la grabaciones"
	   fi
	elif [ "$1" = "last" ]
	then
	   if [[ "$PID_FFMPEG" -gt 0 ]]
	   then
	      if kill "$PID_FFMPEG"
	      then
	         notify-send -i messagebox_info "Se ha detenido la última grabación" "PID: $PID_FFMPEG"
	      fi
	   fi
	fi
}

if [ ! -z "$1" ]
then
   killRecord $1
else
   notify-send -i messagebox_info "Se necesita una opción para detener la grabación"
fi
