Grupo: Internet
Elemento: mutt
Cambio:19-enero-2020

Archivos Originarios: [
  "/home/hakyn/.mutt/hakyn.colors tema",
  "/home/hakyn/.mutt/muttrc"
]

Notas:
 Manejador de correos desde la terminal. Un poco complicado de configurar pero muy bueno en uso. Falta crear archivos aliases (para guardar los correos electronicos) y la carpeta accounts/{alias} (para configurar los passwords y accesos a las plataformas de correo electronico)
